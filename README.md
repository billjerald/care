# Colorblind Accessibility Review and Evaluation Tool (CARE)

![Current Version](https://img.shields.io/badge/version-1.0.0-green.svg) ![OpenCV Version](https://img.shields.io/badge/opencv-4.0.0-green.svg) ![Project Status](https://img.shields.io/badge/status-ongoing-orange.svg) ![Progress](https://img.shields.io/badge/progress-20%25-red.svg)

The **Colorblind Accessibility Review and Evaluation Tool (CARE)** tool is for helping the user interface developers to determine their mobile applications color combination to become visually balancing for the colorblind. It finds out if it is friendly by using image processing and ~~fuzzy logic~~ to gather data and color combinations to compute the percentage of affable. The tool detects the images texts and background color and distinguishes the level of colorblindness.

This is the official repository of the app based on this [thesis](Chapter123_FOR_DecisionTree-FOR-PRINTINGR-1.docx) inspired from this [paper](Colorblind-Affability-Tool-for-Mobile-Applicaiton-User-Interface-2.docx)

---

## Changelog

## 1.0.0

- Initial Release
- Added bug to fix later

---

## Creators

- Jerald Bill Vargas
- Aaron Charles Calinao
- Lalaine Pena
- Sherwin Lacno

---

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)](https://forthebadge.com)