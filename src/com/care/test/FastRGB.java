package com.care.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class FastRGB {
	private int width;
	// private int height;
	private boolean hasAlphaChannel;
	private int pixelLength;
	private byte[] pixels;

	FastRGB(BufferedImage image) {
		pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		width = image.getWidth();
		// height = image.getHeight();
		hasAlphaChannel = image.getAlphaRaster() != null;
		pixelLength = 3;
		if (hasAlphaChannel) {
			pixelLength = 4;
		}

	}

	int getRGB(int x, int y) {
		int pos = (y * pixelLength * width) + (x * pixelLength);

		int argb = -16777216; // 255 alpha
		if (hasAlphaChannel) {
			argb = ((pixels[pos++] & 0xff) << 24); // alpha
		}

		argb += (pixels[pos++] & 0xff); // blue
		argb += ((pixels[pos++] & 0xff) << 8); // green
		argb += ((pixels[pos++] & 0xff) << 16); // red
		return argb;
	}

	public static void main(String[] args) {
		try {
			new FastRGB(ImageIO.read(new File("img/image3.png")));
		} catch (IOException e) {
		}
	}
}