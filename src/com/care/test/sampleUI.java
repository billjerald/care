package com.care.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.LineBorder;

public class sampleUI extends JFrame {
	// private JTextField textField;
	// private JLabel lblFind;

	private Color borderColor = Color.BLACK;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JList list;
	private JLabel lblImages;
	private JLabel label;
	private JPanel panel_1;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					sampleUI frame = new sampleUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public sampleUI() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(sampleUI.class.getResource("/img/icon.png")));
		setSize(1020, 680);
		setLocationRelativeTo(null);
		getContentPane().setBackground(new Color(47, 54, 61));
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(0, 0, 1014, 54);
		panel.setBackground(new Color(29, 33, 37));
		getContentPane().add(panel);

		scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(Color.BLACK));
		scrollPane.setBackground(new Color(36, 41, 46));
		scrollPane.setBounds(0, 53, 244, 598);
		getContentPane().add(scrollPane);

		DefaultListModel listModel = new DefaultListModel();
		for (int i = 0; i < 50; i++) {
			listModel.addElement("Sample" + i + ".jpg");
		}

		list = new JList(listModel);
		UIManager.put("List.focusCellHighlightBorder", BorderFactory.createEmptyBorder());
		list.setSelectionBackground(new Color(47, 54, 61));
		list.setFont(new Font("SansSerif", Font.PLAIN, 12));
		list.setForeground(Color.WHITE);
		list.setBackground(new Color(36, 41, 46));
		scrollPane.setViewportView(list);

		lblImages = new JLabel("Images");
		lblImages.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblImages.setHorizontalAlignment(SwingConstants.CENTER);
		lblImages.setHorizontalTextPosition(SwingConstants.CENTER);
		lblImages.setForeground(Color.WHITE);
		lblImages.setBackground(new Color(36, 41, 46));
		lblImages.setOpaque(true);
		lblImages.setFont(new Font("SansSerif", Font.PLAIN, 12));
		scrollPane.setColumnHeaderView(lblImages);

		panel_1 = new JPanel();
		panel_1.setBackground(new Color(47, 54, 61));
		panel_1.setBounds(243, 55, 771, 596);
		getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		label = new JLabel("");
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(new File("res/img/icon.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// panel_1.add(new ScalablePane(bimg), BorderLayout.CENTER);

		try {
			if (bimg.getHeight() > 480 || bimg.getWidth() > 280) {
				label.setIcon(new ImageIcon(new ImageIcon(new File("res/img/icon.png").toURI().toURL()).getImage()
						.getScaledInstance(280, 480, Image.SCALE_DEFAULT)));
			} else {
				label.setIcon(new ImageIcon(new ImageIcon(new File("res/img/icon.png").toURI().toURL()).getImage()
						.getScaledInstance(bimg.getWidth(), bimg.getHeight(), Image.SCALE_DEFAULT)));
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setPreferredSize(new Dimension(280, 480));
		panel_1.add(label, BorderLayout.WEST);

		lblNewLabel = new JLabel("Title");
		lblNewLabel.setBorder(null);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 26));
		panel_1.add(lblNewLabel, BorderLayout.NORTH);

		/*
		 * textField = new JTextField(20) {
		 *
		 * @Override protected void paintComponent(Graphics g) { if (!isOpaque() &&
		 * getBorder() instanceof RoundedCornerBorder) { Graphics2D g2 = (Graphics2D)
		 * g.create(); g2.setPaint(getBackground()); g2.fill(((RoundedCornerBorder)
		 * getBorder()).getBorderShape(0, 0, getWidth() - 1, getHeight() - 1));
		 * g2.dispose(); } super.paintComponent(g); }
		 *
		 * @Override public void updateUI() { super.updateUI(); setOpaque(false);
		 * setBorder(new RoundedCornerBorder()); } }; textField.addFocusListener(new
		 * FocusAdapter() {
		 *
		 * @Override public void focusGained(FocusEvent e) { borderColor = new Color(3,
		 * 102, 214); textField.setBorder(new RoundedCornerBorder()); }
		 *
		 * @Override public void focusLost(FocusEvent e) { borderColor = Color.BLACK;
		 * textField.setBorder(new RoundedCornerBorder()); } });
		 * textField.setCaretColor(Color.WHITE); textField.setBackground(new Color(36,
		 * 41, 46)); textField.setBounds(84, 477, 725, 31);
		 * getContentPane().add(textField);
		 *
		 * lblFind = new JLabel(""); lblFind.setIcon(new
		 * ImageIcon(sampleUI.class.getResource("/img/find.JPG")));
		 * lblFind.setBounds(224, 94, 560, 305); getContentPane().add(lblFind);
		 */
	}

	class RoundedCornerBorder extends AbstractBorder {
		private final Color ALPHA_ZERO = new Color(0x0, true);

		@Override
		public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
			Graphics2D g2 = (Graphics2D) g.create();
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			Shape border = getBorderShape(x, y, width - 1, height - 1);
			g2.setPaint(ALPHA_ZERO);
			Area corner = new Area(new Rectangle2D.Double(x, y, width, height));
			corner.subtract(new Area(border));
			g2.fill(corner);
			g2.setPaint(borderColor);
			g2.draw(border);
			g2.dispose();
		}

		public Shape getBorderShape(int x, int y, int w, int h) {
			int r = h; // h / 2;
			return new RoundRectangle2D.Double(x, y, w, h, r, r);
		}

		@Override
		public Insets getBorderInsets(Component c) {
			return new Insets(4, 8, 4, 8);
		}

		@Override
		public Insets getBorderInsets(Component c, Insets insets) {
			insets.set(4, 8, 4, 8);
			return insets;
		}
	}
}
