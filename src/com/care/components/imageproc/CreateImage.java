package com.care.components.imageproc;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CreateImage {

	public CreateImage(File filename, Color foreground, Color background, String text, Font font) {

		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/Open_Sans/OpenSans-Regular.ttf")));
		} catch (IOException | FontFormatException e) {
		}
		int width = 350;
		int height = 100;

		BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2d = bufferedImage.createGraphics();

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

		g2d.setComposite(AlphaComposite.Clear);
		g2d.fillRect(0, 0, width, height);

		g2d.setComposite(AlphaComposite.Src);
		g2d.setColor(background);
		g2d.fillRoundRect(0, 0, width, height, 50, 50);

		FontMetrics metrics = g2d.getFontMetrics(font);
		int x = (width - metrics.stringWidth(text)) / 2;
		int y = ((height - metrics.getHeight()) / 2) + metrics.getAscent();
		g2d.setColor(foreground);
		g2d.setFont(font);
		g2d.drawString(text, x, y);

		g2d.dispose();

		// Image trasnparentImage = new
		// ImageProc().TransformGrayToTransparency(bufferedImage);
		// bufferedImage = new ImageProc().ImageToBufferedImage(trasnparentImage,
		// bufferedImage.getWidth(),
		// bufferedImage.getHeight());

		try {
			File file = filename;
			boolean bool = file.createNewFile();
			file.delete();
			ImageIO.write(bufferedImage, "png", file);
		} catch (Exception e) {
		}
	}
}