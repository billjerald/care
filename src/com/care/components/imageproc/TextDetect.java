package com.care.components.imageproc;

import static org.opencv.core.Core.FILLED;
import static org.opencv.core.Core.countNonZero;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;

public class TextDetect {

	ImageProc imageProc = new ImageProc();

	public List<Rect> detectWords(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		Mat small = new Mat();

		/*Imgproc.pyrDown(img,img);*/
		//Converts img from one color space to another.
		Imgproc.cvtColor(img, small, Imgproc.COLOR_BGR2GRAY);

		saveAsImage(small, "A2.Imgproc.COLOR_BGR2GRAY");

		//adaptive tresh
		Mat adaptiveTresh = new Mat();
		Imgproc.adaptiveThreshold(small, adaptiveTresh, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 35, 8);
		saveAsImage(adaptiveTresh, "A3.Imgproc.adaptiveTresh");


		Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));


		/*Mat morphKernel2 = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
		//dilate
		Mat dilate = new Mat();
		Imgproc.dilate(grad,dilate,morphKernel2);
		saveAsImage(dilate, "B3.Imgproc.dilate");*/

		// binarize
		Mat bw = new Mat();

		Imgproc.threshold(adaptiveTresh, bw, 0.0, 255.0, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
		//Imgproc.adaptiveThreshold(grad, bw, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_OTSU, 11, 12);

		saveAsImage(bw, "C2.Imgproc.threshold");

		// connect horizontal oriented regions
		Mat connected = new Mat();

		morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 1));
		Imgproc.morphologyEx(bw, connected, Imgproc.MORPH_CLOSE, morphKernel);

		saveAsImage(connected, "D2.Imgproc.MORPH_CLOSE");

		// find contours
		Mat mask = Mat.zeros(connected.size(), CvType.CV_8UC1);

		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(connected, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE,
				new Point(0, 0));


		// filter contours
		for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
			Rect rect = Imgproc.boundingRect(contours.get(contourIdx));
			Mat maskROI = new Mat(mask, rect);
			maskROI.setTo(new Scalar(0, 0, 0));

			// fill the contour
			Imgproc.drawContours(mask, contours, contourIdx, new Scalar(255, 255, 255), FILLED);

			/*if(contourIdx == contours.size() - 1) {
				saveAsImage(mask, "E2" + contourIdx + ".Imgproc.drawContours");
			}*/

			// ratio ng non-zero pixels sa filled region
			double r = (double) countNonZero(maskROI) / (rect.width * rect.height);
			// HighGui.imshow("contour", mask);
			// HighGui.waitKey(100);

			if (r > .45 // area filled if it contains text
					&& (rect.height > 8 && rect.width > 8) /* constraints on region size */
					) {
				boundRect.add(rect);
			}
		}

		return boundRect;
	}


	// Scalar constants
	private static final Scalar WHITE = new Scalar(255);
	private static final Scalar BLACK = new Scalar(0);
	private static final Scalar BLUE = new Scalar(0, 0, 255);
	private static final Scalar GREEN = new Scalar(0, 255, 0);
	private static final Scalar PURPLE = new Scalar(128, 0, 128);
	private static final Scalar DARK_RED = new Scalar(128, 0, 0);
	private static final Scalar DARK_GREEN = new Scalar(0, 128, 0);

	// Structuring element constants
	private static final Mat KERNEL_3X3 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
	private static final Mat KERNEL_15X15 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15));
	private static final Mat KERNEL_30X30 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(30, 30));

	public List<Rect> detectWordsB(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		Mat mat = new Mat();

		/*Imgproc.pyrDown(img,img);*/
		//Converts img from one color space to another.
		Imgproc.cvtColor(img, mat, Imgproc.COLOR_BGR2GRAY);

		final Mat aux = new Mat(mat.size(), CvType.CV_8UC1);
		final Mat binary = new Mat(mat.size(), CvType.CV_8UC1);


		saveAsImage(mat, "A2.Imgproc.COLOR_BGR2GRAY");

		//adaptive tresh
		Imgproc.adaptiveThreshold(mat, binary, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY, 51, 13);
		saveAsImage(binary, "A3.Imgproc.adaptiveTresh");


		//edge detection
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, KERNEL_3X3);
		Imgproc.morphologyEx(mat, aux, Imgproc.MORPH_CLOSE, KERNEL_3X3);
		Core.addWeighted(mat, 0.5, aux, 0.5, 0, mat);
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_GRADIENT, KERNEL_3X3);
		Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		saveAsImage(mat, "A4.Edges");

		// connect horizontal oriented regions
		Mat connected = new Mat();

		Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 1));
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, morphKernel);

		saveAsImage(mat, "A5.Imgproc.MORPH_CLOSE");

		// Extract word level connected-components from the dilated edge map
		Imgproc.dilate(mat, mat, KERNEL_3X3);
		saveAsImage(mat, "A6.Dilated");
		final List<MatOfPoint> wordCCs = new ArrayList<MatOfPoint>();
		Imgproc.findContours(mat, wordCCs, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		// Filter word level connected-components individually and calculate their average attributes
		final List<MatOfPoint> individuallyFilteredWordCCs = new ArrayList<MatOfPoint>();
		final List<MatOfPoint> removedWordCCs = new ArrayList<MatOfPoint>();
		double avgWidth = 0, avgHeight = 0, avgArea = 0;
		for (MatOfPoint cc : wordCCs) {
			final Rect boundingBox = Imgproc.boundingRect(cc);
			if (boundingBox.height >= 6 // bounding box height >= 6
					&& boundingBox.area() >= 50 // bounding box area >= 50
					&& (double) boundingBox.width / (double) boundingBox.height >= 0.25 // bounding box aspect ratio >= 1:4
					&& boundingBox.width <= 0.75 * mat.width() // bounding box width <= 0.75 image width
					&& boundingBox.height <= 0.75 * mat.height()) // bounding box height <= 0.75 image height
			{
				individuallyFilteredWordCCs.add(cc);
				avgWidth += boundingBox.width;
				avgHeight += boundingBox.height;
				avgArea += boundingBox.area();
				boundRect.add(boundingBox);
			} else {
				//removedWordCCs.add(cc);
			}
		}
		wordCCs.clear();
		avgWidth /= individuallyFilteredWordCCs.size();
		avgHeight /= individuallyFilteredWordCCs.size();
		avgArea /= individuallyFilteredWordCCs.size();

/*			if (DEBUG) {
			Imgproc.drawContours(debugMat, removedWordCCs, -1, BLUE, -1);
			removedWordCCs.clear();
		}*/


		// Filter word level connected-components in relation to their average attributes
		final List<MatOfPoint> filteredWordCCs = new ArrayList<MatOfPoint>();
		for (MatOfPoint cc : individuallyFilteredWordCCs) {
			final Rect boundingBox = Imgproc.boundingRect(cc);
			if (boundingBox.width >= 0.125 * avgWidth // bounding box width >= 0.125 average width
					&& boundingBox.width <= 8 * avgWidth // bounding box width <= 8 average width
					&& boundingBox.height >= 0.25 * avgHeight // bounding box height >= 0.25 average height
					&& boundingBox.height <= 4 * avgHeight) // bounding box height <= 4 average height
			{
				filteredWordCCs.add(cc);
				boundRect.add(boundingBox);
			} else {
				// removedWordCCs.add(cc);
			}
		}

		individuallyFilteredWordCCs.clear();
		/*if (DEBUG) {
			Imgproc.drawContours(debugMat, filteredWordCCs, -1, GREEN, -1);
			Imgproc.drawContours(debugMat, removedWordCCs, -1, PURPLE, -1);
			removedWordCCs.clear();
		}*/

		// Extract paragraph level connected-components
		mat.setTo(BLACK);
		Imgproc.drawContours(mat, filteredWordCCs, -1, WHITE, -1);
		final List<MatOfPoint> paragraphCCs = new ArrayList<MatOfPoint>();
		Imgproc.morphologyEx(mat, aux, Imgproc.MORPH_CLOSE, KERNEL_30X30);
		Imgproc.findContours(aux, paragraphCCs, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		// Filter paragraph level connected-components according to the word level connected-components inside
		final List<MatOfPoint> textCCs = new ArrayList<MatOfPoint>();
		for (MatOfPoint paragraphCC : paragraphCCs) {
			final List<MatOfPoint> wordCCsInParagraphCC = new ArrayList<MatOfPoint>();
			aux.setTo(BLACK);
			Imgproc.drawContours(aux, Collections.singletonList(paragraphCC), -1, WHITE, -1);
			Core.bitwise_and(mat, aux, aux);
			Imgproc.findContours(aux, wordCCsInParagraphCC, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			final Rect boundingBox = Imgproc.boundingRect(paragraphCC);
			final double center = mat.size().width / 2;
			final double distToCenter = center > boundingBox.x + boundingBox.width ? center - boundingBox.x - boundingBox.width : center < boundingBox.x ? boundingBox.x - center : 0.0;
/*			if (DEBUG) {
				System.err.println("****************************************");
				System.err.println("\tArea:                " + boundingBox.area());
				System.err.println("\tDistance to center:  " + distToCenter);
				System.err.println("\tCCs inside:          " + wordCCsInParagraphCC.size());
			}*/
			if ((wordCCsInParagraphCC.size() >= 10 || wordCCsInParagraphCC.size() >= 0.3 * filteredWordCCs.size()) && mat.size().width / distToCenter >= 4) {
				textCCs.addAll(wordCCsInParagraphCC);
/*				if (DEBUG) {
					System.err.println("\tText:                YES");
					Imgproc.drawContours(debugMat, Collections.singletonList(paragraphCC), -1, DARK_GREEN, 5);
				}*/
			} else {
/*				if (DEBUG) {
					System.err.println("\tText:                NO");
					Imgproc.drawContours(debugMat, Collections.singletonList(paragraphCC), -1, DARK_RED, 5);
				}*/
			}
		}

		filteredWordCCs.clear();
		paragraphCCs.clear();
		mat.setTo(WHITE);
		Imgproc.drawContours(mat, textCCs, -1, BLACK, -1);
		textCCs.clear();


		// Obtain the final text mask from the filtered connected-components
		Imgproc.erode(mat, mat, KERNEL_15X15);
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, KERNEL_30X30);

		// Apply the text mask to the binarized image
		binary.setTo(WHITE, mat);

/*		// Dewarp the text using Leptonica
		Pix pixs = Image.fromMat(binary).toGrayscalePix();
		Pix pixsDewarp = Dewarp.dewarp(pixs, 0, Dewarp.DEFAULT_SAMPLING, 5, true);
		final Image result = Image.fromGrayscalePix(pixsDewarp);
		if (DEBUG) result.write(new File(debugDir, "8_dewarp.jpg"));*/


		mat.release();
		aux.release();
		binary.release();


		return boundRect;
	}

	public List<Rect> detectWordsOrig(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		Mat small = new Mat();

		/*Imgproc.pyrDown(img,img);*/
		//Converts img from one color space to another.
		Imgproc.cvtColor(img, small, Imgproc.COLOR_BGR2GRAY);

		saveAsImage(small, "A.Imgproc.COLOR_BGR2GRAY");

		// gradient
		//In mathematical morphology, a structuring element is a shape, used to probe or interact with a given image, with the purpose of drawing conclusions on how this shape fits or misses the shapes in the image.
		Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));

		Mat grad = new Mat();
		Imgproc.morphologyEx(small, grad, Imgproc.MORPH_GRADIENT, morphKernel);
		saveAsImage(grad, "B.Imgproc.MORPH_GRADIENT");


		// binarize
		Mat bw = new Mat();
		Imgproc.threshold(grad, bw, 0.0, 255.0, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
		saveAsImage(bw, "C.Imgproc.threshold");

		// connect horizontal oriented regions
		Mat connected = new Mat();

		morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 1));
		Imgproc.morphologyEx(bw, connected, Imgproc.MORPH_CLOSE, morphKernel);

		saveAsImage(connected, "D.Imgproc.MORPH_CLOSE");

		// find contours
		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);

		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(connected, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE,
				new Point(0, 0));


		// filter contours
		for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
			Rect rect = Imgproc.boundingRect(contours.get(contourIdx));
			Mat maskROI = new Mat(mask, rect);
			maskROI.setTo(new Scalar(0, 0, 0));

			// fill the contour
			Imgproc.drawContours(mask, contours, contourIdx, new Scalar(255, 255, 255), FILLED);

			/*if(contourIdx == contours.size() - 1) {
				saveAsImage(mask, "E2" + contourIdx + ".Imgproc.drawContours");
			}*/

			// ratio ng non-zero pixels sa filled region
			double r = (double) countNonZero(maskROI) / (rect.width * rect.height);
			// HighGui.imshow("contour", mask);
			// HighGui.waitKey(100);

			if (r > .45 // area filled if it contains text
					&& (rect.height > 8 && rect.width > 8) /* constraints on region size */
					) {
				boundRect.add(rect);
			}
		}

		return boundRect;
	}

	public List<Rect> detectLetters(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		Mat img_gray = new Mat(), img_sobel = new Mat(), img_threshold = new Mat(), element = new Mat();
		Imgproc.cvtColor(img, img_gray, Imgproc.COLOR_RGB2GRAY);
		Imgproc.Sobel(img_gray, img_sobel, CvType.CV_8U, 1, 0, 3, 1, 0, Core.BORDER_DEFAULT);
		// at src, Mat dst, double thresh, double maxval, int type
		Imgproc.threshold(img_sobel, img_threshold, 0, 255, Imgproc.THRESH_OTSU + Imgproc.THRESH_BINARY);
		element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(17, 3));
		Imgproc.morphologyEx(img_threshold, img_threshold, Imgproc.MORPH_CLOSE, element);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(img_threshold, contours, hierarchy, 0, 1);

		// List<MatOfPoint> contours_poly = new ArrayList<MatOfPoint>(contours.size());

		for (int i = 0; i < contours.size(); i++) {

			MatOfPoint2f mMOP2f1 = new MatOfPoint2f();
			MatOfPoint2f mMOP2f2 = new MatOfPoint2f();

			contours.get(i).convertTo(mMOP2f1, CvType.CV_32FC2);
			Imgproc.approxPolyDP(mMOP2f1, mMOP2f2, 2, true);
			mMOP2f2.convertTo(contours.get(i), CvType.CV_32S);

			Rect appRect = Imgproc.boundingRect(contours.get(i));
			if (appRect.width > appRect.height) {
				boundRect.add(appRect);
			}
		}

		return boundRect;
	}

	public List<Rect> detectLetters2(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		Mat small = new Mat();
		Mat grad = new Mat();

		/* Imgproc.pyrDown(img,rgb); */
		Imgproc.cvtColor(img, small, Imgproc.COLOR_BGR2GRAY);

		Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
		Imgproc.morphologyEx(small, grad, Imgproc.MORPH_GRADIENT, morphKernel);

		Mat bw = new Mat();

		Imgproc.threshold(grad, bw, 0.0, 255.0, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);

		Mat connected = new Mat();

		morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 1));
		Imgproc.morphologyEx(bw, connected, Imgproc.MORPH_CLOSE, morphKernel);

		Mat mask = Mat.zeros(bw.size(), CvType.CV_8UC1);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(connected, contours, hierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE,
				new Point(0, 0));

		for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
			Rect rect = Imgproc.boundingRect(contours.get(contourIdx));
			Mat maskROI = new Mat(mask, rect);
			maskROI.setTo(new Scalar(0, 0, 0));

			Imgproc.drawContours(mask, contours, contourIdx, new Scalar(255, 255, 255), FILLED);

			double r = (double) countNonZero(maskROI) / (rect.width * rect.height);

			if (r > .45 && (rect.height > 8 && rect.width > 8)) {
				boundRect.add(rect);
			}
		}

		return boundRect;
	}

	public void saveAsImage(Mat img, String filename) {

		BufferedImage bufferedImage = null;
		try {
			bufferedImage = mat2BufferedImage(img);
		} catch (Exception ex) {
		}

		String path = imageProc.getPath();
		String imageFormat = imageProc.getImageFormat();
		File outputfile;
		File submatFilePath = new File(new File(path).getParentFile().toString() + "//imageProcFiles//"
				+ stripExtension(new File(path).getName()));
		try {
			submatFilePath.mkdirs();
			outputfile = new File(
					submatFilePath.getAbsolutePath() + "//" + filename + "." + imageFormat);
			ImageIO.write(bufferedImage, imageFormat, outputfile);

		} catch (IOException io) {
		}
	}

	public String stripExtension(String str) {
		// Handle null case specially.

		/*
		 * if (str == null) return str;
		 *
		 * // Get position of last '.'.
		 */

		int pos = str.lastIndexOf(".");

		// If there wasn't any '.' just return the string as is.

		if (pos == -1) {
			return str;
		}

		// Otherwise return the string, up to the dot.

		return str.substring(0, pos);
	}

	static BufferedImage mat2BufferedImage(Mat matrix) throws Exception {
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(".jpg", matrix, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));
		return bi;
	}


	public List<Rect> segmentText(Mat img) {
		List<Rect> boundRect = new ArrayList<>();

		saveAsImage(img, "A1.Original");
		Mat mat = new Mat();

		/*Imgproc.pyrDown(img,img);*/
		//Converts img from one color space to another.
		Imgproc.cvtColor(img, mat, Imgproc.COLOR_BGR2GRAY);

		final Mat aux = new Mat(mat.size(), CvType.CV_8UC1);
		final Mat binary = new Mat(mat.size(), CvType.CV_8UC1);


		saveAsImage(mat, "A2.Imgproc.COLOR_BGR2GRAY");

		//adaptive tresh
		Imgproc.adaptiveThreshold(mat, binary, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 15, 3);
		saveAsImage(binary, "A3.Imgproc.adaptiveTresh");


		//edge detection
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_OPEN, KERNEL_3X3);
		saveAsImage(mat, "A4A.Imgproc.MORPH_OPEN");

		Imgproc.morphologyEx(mat, aux, Imgproc.MORPH_CLOSE, KERNEL_3X3);
		saveAsImage(aux, "A4B.Imgproc.MORPH_CLOSE");

		Core.addWeighted(mat, 0.5, aux, 0.5, 0, mat);
		saveAsImage(mat, "A4D.add.mat_aux");

		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_GRADIENT, KERNEL_3X3);
		saveAsImage(mat, "A4C.Imgproc.MORPH_GRADIENT");



		Imgproc.threshold(mat, mat, 0, 255, Imgproc.THRESH_BINARY | Imgproc.THRESH_OTSU);
		saveAsImage(mat, "A4E.Edges");

		// connect horizontal oriented regions
		Mat connected = new Mat();

		Mat morphKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 1));
		Imgproc.morphologyEx(mat, mat, Imgproc.MORPH_CLOSE, morphKernel);

		saveAsImage(mat, "A5.Imgproc.MORPH_CLOSE");

		// Extract word level connected-components from the dilated edge map
		Imgproc.dilate(mat, mat, KERNEL_3X3);
		saveAsImage(mat, "A6.Dilated");
		final List<MatOfPoint> wordCCs = new ArrayList<MatOfPoint>();
		Imgproc.findContours(mat, wordCCs, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		// Filter word level connected-components individually and calculate their average attributes
		final List<MatOfPoint> individuallyFilteredWordCCs = new ArrayList<MatOfPoint>();
		final List<MatOfPoint> removedWordCCs = new ArrayList<MatOfPoint>();
		double avgWidth = 0, avgHeight = 0, avgArea = 0;
		for (MatOfPoint cc : wordCCs) {
			final Rect boundingBox = Imgproc.boundingRect(cc);
			if (boundingBox.height >= 6 // bounding box height >= 6
					&& boundingBox.area() >= 50 // bounding box area >= 50
					&& (double) boundingBox.width / (double) boundingBox.height >= 0.25 // bounding box aspect ratio >= 1:4
					&& boundingBox.width <= 0.75 * mat.width() // bounding box width <= 0.75 image width
					&& boundingBox.height <= 0.75 * mat.height()) // bounding box height <= 0.75 image height
			{
				individuallyFilteredWordCCs.add(cc);
				avgWidth += boundingBox.width;
				avgHeight += boundingBox.height;
				avgArea += boundingBox.area();
				boundRect.add(boundingBox);
			}
		}
		wordCCs.clear();
		avgWidth /= individuallyFilteredWordCCs.size();
		avgHeight /= individuallyFilteredWordCCs.size();
		avgArea /= individuallyFilteredWordCCs.size();


		// Filter word level connected-components in relation to their average attributes
		final List<MatOfPoint> filteredWordCCs = new ArrayList<MatOfPoint>();
		for (MatOfPoint cc : individuallyFilteredWordCCs) {
			final Rect boundingBox = Imgproc.boundingRect(cc);
			if (boundingBox.width >= 0.125 * avgWidth // bounding box width >= 0.125 average width
					&& boundingBox.width <= 8 * avgWidth // bounding box width <= 8 average width
					&& boundingBox.height >= 0.25 * avgHeight // bounding box height >= 0.25 average height
					&& boundingBox.height <= 4 * avgHeight) // bounding box height <= 4 average height
			{
				filteredWordCCs.add(cc);
				boundRect.add(boundingBox);
			}
		}

		individuallyFilteredWordCCs.clear();


		// Extract paragraph level connected-components
		mat.setTo(BLACK);
		Imgproc.drawContours(mat, filteredWordCCs, -1, WHITE, -1);
		final List<MatOfPoint> paragraphCCs = new ArrayList<MatOfPoint>();
		Imgproc.morphologyEx(mat, aux, Imgproc.MORPH_CLOSE, KERNEL_30X30);
		Imgproc.findContours(aux, paragraphCCs, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		// Filter paragraph level connected-components according to the word level connected-components inside
		final List<MatOfPoint> textCCs = new ArrayList<MatOfPoint>();
		for (MatOfPoint paragraphCC : paragraphCCs) {
			final List<MatOfPoint> wordCCsInParagraphCC = new ArrayList<MatOfPoint>();
			aux.setTo(BLACK);
			Imgproc.drawContours(aux, Collections.singletonList(paragraphCC), -1, WHITE, -1);
			Core.bitwise_and(mat, aux, aux);
			Imgproc.findContours(aux, wordCCsInParagraphCC, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
			final Rect boundingBox = Imgproc.boundingRect(paragraphCC);
			final double center = mat.size().width / 2;
			final double distToCenter = center > boundingBox.x + boundingBox.width ? center - boundingBox.x - boundingBox.width : center < boundingBox.x ? boundingBox.x - center : 0.0;

			if ((wordCCsInParagraphCC.size() >= 10 || wordCCsInParagraphCC.size() >= 0.3 * filteredWordCCs.size()) && mat.size().width / distToCenter >= 4) {
				textCCs.addAll(wordCCsInParagraphCC);

			}
		}

			filteredWordCCs.clear();
			paragraphCCs.clear();

			textCCs.clear();


			mat.release();
			aux.release();
			binary.release();


			return boundRect;


	}
}
