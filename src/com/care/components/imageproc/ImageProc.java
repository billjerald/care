package com.care.components.imageproc;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import com.care.components.coloranalysis.Compute;
import com.care.components.coloranalysis.DominantColor;

public class ImageProc {
	Mat img_gray;
	Mat imageRect;
	Imgproc imgproc = new Imgproc();
	Size size = new Size();
	int thresh = 100;
	int elements = 0;
	int colorDifference;
	int dominantColor1[];
	int dominantColor2[];
	double result;
	double average = 0.0;
	double luminosityContrastRatio;
	ArrayList<int[]> color1List = new ArrayList<>();
	ArrayList<int[]> color2List = new ArrayList<>();
	ArrayList<Integer> colorBDList = new ArrayList<>();
	ArrayList<Integer> colorDList = new ArrayList<>();
	ArrayList<Double> colorLCRList = new ArrayList<>();
	ArrayList<Double> resultList = new ArrayList<>();

	ArrayList<String> resultListDeuteranomaly = new ArrayList<>();
	ArrayList<String> resultListProtanomaly = new ArrayList<>();

	static int updateBar = 0;
	private static String path = "";
	private static String imageFormat = "";

	public void processImage(String path, String imageFormat) {

		File file = new File(path);

		this.path = path;
		this.imageFormat = imageFormat;

		// Creata a Matrix of an image
		Mat imgOrig = addImage(path);
		Mat rgbTemp = imgOrig.clone();
		Mat imgRect = imgOrig.clone();
		// Blur it down.. Idk why?
		Imgproc.pyrDown(imgOrig, imgOrig);

		// Create a copy of the original matrix to another
		Mat rgb = imgOrig;

		// Create a copy of an object to another
		// Mat imgTemp = rgb.clone();

		// For apply rects to text
		// Mat imgRect = rgb.clone();

		int optimizeRects = 97;

		TextDetect textDetect = new TextDetect();
		List<Rect> rect = new ArrayList<>();

		rect = textDetect.segmentText(rgb);

		List<Rect> resizedRect = new ArrayList<>();

		for (int i = 0; i < rect.size(); i++) {

			for (int j = 0; j < rect.size(); j++) {
				if (isOverlapping(rect.get(j), rect.get(i)) && j != i) {
					rect.remove(j);
				} else if (isOverlapping(rect.get(i), rect.get(j)) && j != i) {
					rect.remove(i);
				}
			}

			updateBar = (int) ((i / (double) rect.size()) * 100.0);
			resizedRect.add(resizeRectangle(rect.get(i), optimizeRects, optimizeRects));

			// upscale the rectangles for application in the original image
			rect.get(i).x = rect.get(i).x * 2;
			rect.get(i).y = rect.get(i).y * 2;
			rect.get(i).width = rect.get(i).width * 2;
			rect.get(i).height = rect.get(i).height * 2;

			// create submat base on rect
			Mat subMat = new Mat(rgbTemp, resizedRect.get(i));

			BufferedImage bufferedImage = null;
			try {
				bufferedImage = mat2BufferedImage(subMat);
			} catch (Exception ex) {
			}

			DominantColor dominantColor = new DominantColor();
			dominantColor1 = dominantColor.getColor(bufferedImage, 10, false, 1);
			dominantColor2 = dominantColor.getColor(bufferedImage, 10, false, 2);

			int match = 0;
			for (int dColor = 0; dColor < dominantColor1.length; dColor++) {

				if (dominantColor1[dColor] > 255) {
					dominantColor1[dColor] = 255;
				}

				if (dominantColor2[dColor] > 255) {
					dominantColor2[dColor] = 255;
				}

				if (dominantColor1[dColor] == dominantColor2[dColor]) {
					match++;
				} else if (Math.abs(dominantColor1[dColor] - dominantColor2[dColor]) < 4) {
					match++;
				}
			}
			if (match == 3) {
				rect.remove(i);
				resizedRect.remove(i);
				i--;
				continue;
			}

			color1List.add(dominantColor1);
			color2List.add(dominantColor2);

			// apply rect to image
			Imgproc.rectangle(imgRect, rect.get(i).tl(), rect.get(i).br(), new Scalar(0, 255, 0), 1, 8, 0);
			textDetect.saveAsImage(imgRect, "F2.ImgProc.rectangle");

			File outputfile;
			File submatFilePath = new File(new File(path).getParentFile().toString() + "//submatFiles//"
					+ stripExtension(new File(path).getName()));
			try {
				submatFilePath.mkdirs();
				outputfile = new File(
						submatFilePath.getAbsolutePath() + "//submat" + Integer.toString(i) + "." + imageFormat);
				ImageIO.write(bufferedImage, imageFormat, outputfile);

			} catch (IOException io) {
			}

			// compute for color brightness
			Compute compute = new Compute();
			int colorBrightnessDifference = compute.colorBrightnessDifferenceW3C(dominantColor1, dominantColor2);
			colorDifference = compute.colorDifference(dominantColor1, dominantColor2);
			luminosityContrastRatio = compute.luminosityContrastRatioW3C(dominantColor1, dominantColor2);
			colorBDList.add(colorBrightnessDifference);
			colorDList.add(colorDifference);
			colorLCRList.add(luminosityContrastRatio);

			/*
			 * FuzzyLogic fuzzyLogic = new FuzzyLogic(); resultList.add(
			 * fuzzyLogic.colorAccesFuzzy(luminosityContrastRatio, colorDifference,
			 * colorBrightnessDifference));
			 *
			 * average += resultList.get(i);
			 */
			String lcr = null, cd = null, cbd;

			if (luminosityContrastRatio <= 2.25) {
				lcr = "very low";
			} else if (luminosityContrastRatio <= 4.49) {
				lcr = "low";
			} else if (luminosityContrastRatio <= 6.75) {
				lcr = "normal";
			} else if (luminosityContrastRatio <= 9) {
				lcr = "high";
			} else if (luminosityContrastRatio > 9) {
				lcr = "very high";
			}

			if (colorDifference <= 249) {
				cd = "very low";
			} else if (colorDifference <= 499) {
				cd = "low";
			} else if (colorDifference <= 550) {
				cd = "normal";
			} else if (colorDifference <= 800) {
				cd = "high";
			} else if (colorDifference > 800) {
				cd = "very high";
			}

			if (colorBrightnessDifference <= 62) {
				cbd = "very low";
			} else if (colorBrightnessDifference <= 124) {
				cbd = "low";
			} else if (colorBrightnessDifference <= 150) {
				cbd = "normal";
			} else if (colorBrightnessDifference <= 212) {
				cbd = "high";
			} else if (colorBrightnessDifference > 212) {
				cbd = "very high";
			}

			if (cd.equals("very low")) {
				resultListDeuteranomaly.add("Fail");
			} else if (cd.equals("low") || cd.equals("normal") || cd.equals("high") || cd.equals("very high")) {
				resultListDeuteranomaly.add("Pass");
			}

			if (lcr.equals("very low")) {
				resultListProtanomaly.add("Fail");
			} else if (lcr.equals("low") || lcr.equals("normal") || lcr.equals("high") || lcr.equals("very high")) {
				resultListProtanomaly.add("Pass");
			}
		}

		imageRect = imgRect.clone();
		average /= rect.size();
		elements = rect.size();
	}

	public double getLuminosityContrast() {
		return luminosityContrastRatio;
	}

	public double getAverage() {
		return average;
	}

	public int getColorDifference() {
		return colorDifference;
	}

	public int getElements() {
		return elements;
	}

	public ArrayList<int[]> getColor1() {
		return color1List;
	}

	public ArrayList<int[]> getColor2() {
		return color2List;
	}

	public ArrayList<Integer> getColorBDList() {
		return colorBDList;
	}

	public ArrayList<Integer> getColorDList() {
		return colorDList;
	}

	public ArrayList<Double> getColorLCRList() {
		return colorLCRList;
	}

	/*
	 * public ArrayList<Double> getResultList() { return resultList; } edited for
	 * Pass or Fail output
	 */

	public ArrayList<String> getResultListDeuteranomaly() {
		return resultListDeuteranomaly;
	}

	public ArrayList<String> getResultListProtanomaly() {
		return resultListProtanomaly;
	}

	public Mat getImageRect() {
		return imageRect;
	}

	public String stripExtension(String str) {
		// Handle null case specially.

		/*
		 * if (str == null) return str;
		 *
		 * // Get position of last '.'.
		 */

		int pos = str.lastIndexOf(".");

		// If there wasn't any '.' just return the string as is.

		if (pos == -1) {
			return str;
		}

		// Otherwise return the string, up to the dot.

		return str.substring(0, pos);
	}

	public static Mat BufferedImage2Mat(BufferedImage image, String format) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageIO.write(image, format, byteArrayOutputStream);
		byteArrayOutputStream.flush();
		return Imgcodecs.imdecode(new MatOfByte(byteArrayOutputStream.toByteArray()), -1);
	}

	public Mat bufferedImageToMat(BufferedImage bi) {
		Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
		mat.put(0, 0, data);
		return mat;
	}

	public Mat addImage(String imgPath) {
		Mat img;

		img = Imgcodecs.imread(imgPath);
		return img;
	}

	private static Rect resizeRectangle(Rect rect, int heightPercentage, int widthPercetange) {
		int rwidth = rect.width;
		int rheight = rect.height;

		rect.width = Math.round((rect.width * widthPercetange) / 100.0f);
		rect.height = Math.round((rect.height * heightPercentage) / 100.0f);
		rect.x += (rwidth - rect.width) / 2;
		rect.y += (rheight - rect.height) / 2;

		return rect;
	}

	private static Rect expandRectangle(Rect rect, int heightPercentage, int widthPercetange) {
		int rwidth = rect.width;
		int rheight = rect.height;

		rect.width = Math.round((rect.width * widthPercetange) / 100.0f);
		rect.height = Math.round((rect.height * heightPercentage) / 100.0f);

		return rect;
	}

	public Mat resizeImage(Mat image, double scalePercent) {
		scalePercent = scalePercent / 100;

		Mat resImage = new Mat();

		Size sz = new Size(image.width() * scalePercent, image.height() * scalePercent);
		Imgproc.resize(image, resImage, sz);
		return resImage;
	}

	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	/// Convert image to gray and blur it
	public Mat grayAndBlur(Mat img) {
		Imgproc.cvtColor(img, img_gray, Imgproc.COLOR_BGR2GRAY);
		Imgproc.blur(img_gray, img_gray, new Size(3, 3));

		return img;
	}

	public Image matToBufferedImage2(Mat m) {
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (m.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		int bufferSize = m.channels() * m.cols() * m.rows();
		byte[] b = new byte[bufferSize];
		m.get(0, 0, b); // get all the pixels
		BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(b, 0, targetPixels, 0, b.length);
		return image;

	}

	public BufferedImage matToBufferedImage(Mat mat) {
		Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY, 0);

		// Create an empty image in matching format
		BufferedImage gray = new BufferedImage(mat.width(), mat.height(), BufferedImage.TYPE_3BYTE_BGR);

		// Get the BufferedImage's backing array and copy the pixels directly into it
		byte[] data = ((DataBufferByte) gray.getRaster().getDataBuffer()).getData();
		mat.get(0, 0, data);

		return gray;
	}

	static BufferedImage mat2BufferedImage(Mat matrix) throws Exception {
		MatOfByte mob = new MatOfByte();
		Imgcodecs.imencode(".jpg", matrix, mob);
		byte ba[] = mob.toArray();

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(ba));
		return bi;
	}

	public BufferedImage ImageToBufferedImage(Image image, int width, int height) {
		BufferedImage dest = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = dest.createGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
		return dest;
	}

	public Image TransformGrayToTransparency(BufferedImage image) {
		ImageFilter filter = new RGBImageFilter() {
			@Override
			public final int filterRGB(int x, int y, int rgb) {
				return (rgb << 8) & 0xFF000000;
			}
		};

		ImageProducer ip = new FilteredImageSource(image.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(ip);
	}

	public boolean isOverlapping(Rect rectPast, Rect rectNow) {
		if ((rectPast.y <= rectNow.y && rectPast.x <= rectNow.x)
				&& (rectPast.x + rectPast.width >= rectNow.x + rectNow.width
						&& rectPast.y + rectPast.height >= rectNow.y + rectNow.height)) {
			return true;
		} else {
			return false;
		}
	}

	public String getPath() {
		return this.path;
	}

	public String getImageFormat() {
		return this.imageFormat;
	}

}