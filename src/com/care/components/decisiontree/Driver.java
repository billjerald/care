package com.care.components.decisiontree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.stream.IntStream;

/**
 * Created by billJerald on 09/01/2019.
 */
public class Driver {
	// Define Datasets
	static String[][] COLORANALYSIS = { { "LCR", "CBR", "CD", "CA" }, { "fail", "fail", "fail", "fail" },
			{ "fail", "pass", "pass", "pass" }, { "pass", "pass", "pass", "pass" }, { "pass", "pass", "fail", "pass" },
			{ "pass", "pass", "pass", "pass" }, { "pass", "fail", "pass", "fail" } };

	public static void main(String[] args) {
		Driver driver = new Driver();

		HashMap<String, String[][]> datas = new HashMap<String, String[][]>();
		datas.put("COLORANALYSIS", COLORANALYSIS);

		datas.keySet().forEach(data -> {
			HashMap<Feature, Double> featuresInfoGain = new HashMap<Feature, Double>();
			DataSet dataSet = new DataSet(datas.get(data));

			IntStream.range(0, datas.get(data)[0].length - 1).forEach(column -> {
				Feature feature = new Feature(datas.get(data), column);
				ArrayList<DataSet> dataSets = new ArrayList<DataSet>();
				feature.getValues().stream().forEach(
						featureValue -> dataSets.add(driver.createDataSet(featureValue, column, datas.get(data))));

				double summation = 0;
				for (int i = 0; i < dataSets.size(); i++) {
					summation += ((double) (dataSets.get(i).getData().length - 1) / (datas.get(data).length - 1))
							* dataSets.get(i).getEntropy();
				}
				featuresInfoGain.put(feature, dataSet.getEntropy() - summation);
			});
			System.out.println("<" + data + " DATASETS:\n" + dataSet);
			System.out.println(driver.generateInfoGainDisplayTable(featuresInfoGain));
			System.out
					.println("Best feature to split on is " + driver.determineSplitOnFeature(featuresInfoGain) + "\n");
			System.out.println("\n\n");
		});
	}

	DataSet createDataSet(FeatureValue featureValue, int column, String[][] data) {
		String[][] returnData = new String[featureValue.getOccurences() + 1][data[0].length];
		returnData[0][0] = data[0][0];
		int counter = 1;
		for (int row = 1; row < data.length; row++) {
			if (data[row][column] == featureValue.getName()) {
				returnData[counter++] = data[row];
			}
		}
		return new DataSet(deleteColumn(returnData, column));
	}

	Feature determineSplitOnFeature(HashMap<Feature, Double> featuresInfoGain) {
		Feature splitOnFeature = null;
		Iterator<Feature> iterator = featuresInfoGain.keySet().iterator();
		while (iterator.hasNext()) {
			Feature feature = iterator.next();
			if (splitOnFeature == null) {
				splitOnFeature = feature;
			}
			if (featuresInfoGain.get(splitOnFeature) < featuresInfoGain.get(feature)) {
				splitOnFeature = feature;
			}
		}
		return splitOnFeature;
	}

	StringBuffer generateInfoGainDisplayTable(HashMap<Feature, Double> featuresInfoGain) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("Feature          Information Gain\n");
		IntStream.range(0, 38).forEach(i -> stringBuffer.append("-"));
		stringBuffer.append("\n");
		Iterator<Feature> iterator = featuresInfoGain.keySet().iterator();
		while (iterator.hasNext()) {
			Feature feature = iterator.next();
			stringBuffer.append(feature);
			IntStream.range(0, 21 - feature.getName().length()).forEach(i -> stringBuffer.append(" "));
			stringBuffer.append(String.format("%.8f", featuresInfoGain.get(feature)) + "\n");
		}
		return stringBuffer;
	}

	// define helper method
	String[][] deleteColumn(String[][] data, int deleteColumn) {
		String returnData[][] = new String[data.length][data[0].length - 1];
		for (int row = 0; row < data.length; row++) {
			int columnCounter = 0;
			for (int column = 0; column < data[0].length; column++) {
				if (column != deleteColumn) {
					returnData[row][columnCounter++] = data[row][column];
				}
			}
		}
		return returnData;
	}
}
