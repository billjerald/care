package com.care.components.decisiontree.builddecisiontree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 * Created by billJerald on 12/01/2019.
 */
public class Driver extends JFrame {

	private static final long serialVersionUID = 1L;
	// Define Datasets
	private static String[][] COLORANALYSIS = { { "LCR", "CBR", "CD", "CA" }, { "fail", "fail", "fail", "fail" },
			{ "fail", "pass", "pass", "pass" }, { "pass D", "pass", "pass", "pass" },
			{ "pass", "pass", "fail", "pass" }, { "pass", "pass", "pass", "pass" },
			{ "pass", "fail", "pass", "fail" } };

	private static String[][] DEUTERANOMALY = null;
	private static String[][] PROTANOMALY = null;

	/*
	 * static Map<String, String[][]> datas = Collections.unmodifiableMap(new
	 * HashMap<String, String[][]>() { private static final long serialVersionUID =
	 * 1L; { //put("COLORANALYSIS",COLORANALYSIS); put("DEUTERANOMALY",
	 * DEUTERANOMALY); put("PROTANOMALY", PROTANOMALY); } });
	 */

	static String dataKey = Collections.unmodifiableMap(new HashMap<String, String[][]>() {
		private static final long serialVersionUID = 1L;
		{
			// put("COLORANALYSIS",COLORANALYSIS);
			put("DEUTERANOMALY", DEUTERANOMALY);
			put("PROTANOMALY", PROTANOMALY);
		}
	}).keySet().iterator().next();

	void processDataSet(DataSet dataSet, DefaultMutableTreeNode node, String featureValueName) {
		if (dataSet.toString() != null) {
			System.out.println(dataSet);
		}
		if (dataSet.getEntropy() != 0) {
			System.out.println("Best feature to split on is " + dataSet.getSplitOnFeature() + " "
					+ dataSet.getSplitOnFeature().getFeatureValues());
			HashMap<String, DataSet> featureDataSets = new HashMap<String, DataSet>();
			dataSet.getSplitOnFeature().getFeatureValues()
					.forEach(featureValue -> featureDataSets.put(featureValue.getName(),
							dataSet.createDataSet(dataSet.getSplitOnFeature(), featureValue, dataSet.getData())));
			processDataSets(featureDataSets, node);
		} else {
			String[][] data = dataSet.getData();
			String decision = " [" + data[0][data[0].length - 1] + " = " + data[1][data[0].length - 1] + "]";
			node.add(new DefaultMutableTreeNode(featureValueName + " : " + decision));
			System.out.println("Decision ==> " + decision);
		}
	}

	void processDataSets(HashMap<String, DataSet> dataSets, DefaultMutableTreeNode node) {
		dataSets.keySet().forEach(dataSet -> {
			if (dataSets.get(dataSet).getEntropy() != 0) {
				DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(
						dataSet + " : [" + dataSets.get(dataSet).getSplitOnFeature().getName() + "]");
				node.add(newNode);
				processDataSet(dataSets.get(dataSet), newNode, dataSet);
			} else {
				processDataSet(dataSets.get(dataSet), node, dataSet);
			}
		});
	}

	private static void readCSV(String _filename) {
		File dataset = new File(_filename);
		Scanner s = null;
		try {
			boolean bool = dataset.createNewFile();
			s = new Scanner(new BufferedReader(new FileReader(dataset)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		s.useDelimiter(",|\r\n|\r|\n");

		int i = 0;
		String temp = null;

		while (s.hasNext()) {
			temp = s.next();
			temp = s.next();
			temp = s.next();
			temp = s.next();
			temp = s.next();
			i++;
		}
		s.close();

		String[][] deu = new String[i][4];
		String[][] pro = new String[i][4];

		try {
			boolean bool = dataset.createNewFile();
			s = new Scanner(new BufferedReader(new FileReader(dataset)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		s.useDelimiter(",|\r\n|\r|\n");

		i = 0;

		while (s.hasNext()) {
			deu[i][0] = s.next();
			pro[i][0] = deu[i][0];

			deu[i][1] = s.next();
			pro[i][1] = deu[i][1];

			deu[i][2] = s.next();
			pro[i][2] = deu[i][2];

			deu[i][3] = s.next();
			pro[i][3] = s.next();
			i++;
		}
		s.close();

		DEUTERANOMALY = deu;
		PROTANOMALY = pro;
	}

	public static void main(String[] args) throws Exception {
		readCSV("res/CARE_dataset.csv");
		System.out.println("DEUTERANOMALY");
		for (String[] element : DEUTERANOMALY) {
			System.out.print("[");
			for (int j = 0; j < 4; j++) {
				System.out.print(element[j] + ",");
			}
			System.out.println("]");
		}
		System.out.println();
		System.out.println("PROTANOMALY");
		System.out.print("[");
		for (String[] element : PROTANOMALY) {
			for (int j = 0; j < 4; j++) {
				System.out.print(element[j] + ",");
			}
			System.out.println("]");
		}
		Driver driver = new Driver();
		JTree tree = null;
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		boolean flag = true;
		while (flag) {
			System.out.println("> What do you want to do (build tree, choose dataset, exit) ?");
			String command = bufferedReader.readLine();
			switch (command) {
			case "build tree":
				DataSet dataSet = new DataSet(dataKey, Collections.unmodifiableMap(new HashMap<String, String[][]>() {
					private static final long serialVersionUID = 1L;
					{
						// put("COLORANALYSIS",COLORANALYSIS);
						put("DEUTERANOMALY", DEUTERANOMALY);
						put("PROTANOMALY", PROTANOMALY);
					}
				}).get(dataKey));
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(dataSet.getSplitOnFeature().getName());
				driver.processDataSet(dataSet, node, "");

				if (tree != null) {
					driver.remove(tree);
				}

				tree = new JTree(node);
				driver.add(tree);
				driver.setSize(350, 350);
				driver.setTitle(dataKey + " DATASET");
				driver.setVisible(true);
				break;
			case "choose dataset":
				System.out
						.println("> Choose dataset (" + Collections.unmodifiableMap(new HashMap<String, String[][]>() {
							private static final long serialVersionUID = 1L;
							{
								// put("COLORANALYSIS",COLORANALYSIS);
								put("DEUTERANOMALY", DEUTERANOMALY);
								put("PROTANOMALY", PROTANOMALY);
							}
						}).keySet() + "?   ");
				String value = bufferedReader.readLine();
				if (Collections.unmodifiableMap(new HashMap<String, String[][]>() {
					private static final long serialVersionUID = 1L;
					{
						// put("COLORANALYSIS",COLORANALYSIS);
						put("DEUTERANOMALY", DEUTERANOMALY);
						put("PROTANOMALY", PROTANOMALY);
					}
				}).keySet().contains(value)) {
					dataKey = value;
				} else {
					System.out.println("please enter valid dataset name");
				}
				break;
			case "exit":
				flag = false;
				break;
			}
		}
		System.exit(0);
	}
}
