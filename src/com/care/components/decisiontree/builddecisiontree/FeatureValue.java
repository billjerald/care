package com.care.components.decisiontree.builddecisiontree;

/**
 * Created by billJerald on 12/01/2019.
 */
public class FeatureValue {
	private String name;
	private int occurences;

	public FeatureValue(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getOccurences() {
		return occurences;
	}

	public void setOccurences(int occurences) {
		this.occurences = occurences;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		boolean returnValue = true;
		if (object == null || (getClass() != object.getClass())) {
			returnValue = false;
		}
		if (name == null) {
			if (((FeatureValue) object).name != null) {
				returnValue = false;
			} else if (!name.equals(((FeatureValue) object).name)) {
				returnValue = false;
			}
		}
		return returnValue;
	}

	@Override
	public String toString() {
		return name;
	}
}
