package com.care.components.fuzzy;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.rule.Rule;

public class FuzzyLogic {

	public double colorAccesFuzzy(double lunimosityContrastRatio, int colorDifference, int colorBrightnessDifference) {
		String fclPath = "fcl/colorAccess.fcl";
		FIS fis = FIS.load(fclPath, true);

		if (fis == null) {
			System.err.println("Can't load file: '" + fclPath + "'");
			System.exit(1);
		}

		/* fis.chart(); */

		fis.setVariable("luminosityContrastRatio", lunimosityContrastRatio);
		fis.setVariable("fgAndBgColorDifference", colorDifference);
		fis.setVariable("colorBrightnessDifference", colorBrightnessDifference);

		fis.evaluate();

		double result = fis.getVariable("colorAccess").defuzzify();

		// fis.getVariable("colorAccess").chartDefuzzifier(true);

		for (Rule r : fis.getFunctionBlock("color").getFuzzyRuleBlock("No1").getRules()) {
			System.out.println(r);
		}
		System.out.println("result : " + result);

		return result;

	}
}
