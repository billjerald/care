package com.care.components.coloranalysis;

public class Compute {

	public int colorBrightnessDifference(int rgb1[], int rgb2[]) {
		int colorBrightness1 = 0, colorBrightness2 = 0, colorBrightnessDiff = 0;

		colorBrightness1 = (int) Math.sqrt((rgb1[0] * .241) + (rgb1[1] * .691) + (rgb1[2] * .068));

		colorBrightness2 = (int) Math.sqrt((rgb2[0] * .241) + (rgb2[1] * .691) + (rgb2[2] * .068));

		colorBrightnessDiff = Math.abs(colorBrightness1 - colorBrightness2);

		return colorBrightnessDiff;
	}

	public int colorBrightnessDifferenceW3C(int rgb1[], int rgb2[]) {
		/*
		 * ((Red value * 299) + (Green value * 587) + (Blue value * 114)) / 1000 The
		 * range for color brightness difference is 125.
		 */

		double colorBrightness1 = 0f, colorBrightness2 = 0f, colorBrightnessDiff = 0f;

		colorBrightness1 = ((rgb1[0] * 299) + (rgb1[1] * 587) + (rgb1[2] * 114)) / (double) 1000;

		colorBrightness2 = ((rgb2[0] * 299) + (rgb2[1] * 587) + (rgb2[2] * 114)) / (double) 1000;

		colorBrightnessDiff = Math.abs(colorBrightness1 - colorBrightness2);

		return (int) colorBrightnessDiff;
	}

	public int colorDifference(int rgb1[], int rgb2[]) {
		/*
		 * Color difference is determined by the following formula: { (maximum (Red
		 * value 1, Red value 2) - minimum (Red value 1, Red value 2)) + (maximum (Green
		 * value 1, Green value 2) - minimum (Green value 1, Green value 2)) + (maximum
		 * (Blue value 1, Blue value 2) - minimum (Blue value 1, Blue value 2)) }.
		 *
		 * Color difference as permitted by guidelines is 500.
		 */
		double rmean = (rgb1[0] + rgb2[0]) / (double) 2;
		int r = rgb1[0] - rgb2[0];
		int g = rgb1[1] - rgb2[1];
		int b = rgb1[2] - rgb2[2];
		double weightR = 2 + rmean / 256;
		double weightG = 4.0;
		double weightB = 2 + (255 - rmean) / 256;
		return (int) Math.sqrt((weightR * r * r) + (weightG * g * g) + (weightB * b * b));
	}

	public int luminosityContrastRatio(int rgb1[], int rgb2[]) {
		/*
		 * (L1+.05) / (L2+.05) where L is luminosity and is defined as .2126*R + .7152*G
		 * + .0722B using linearised R, G, and B values. Linearised R (for example) =
		 * (R/FS)^2.2 where FS is full scale value (255 for 8 bit color channels). L1 is
		 * the higher value (of text or background) and L2 is the lower value.
		 *
		 * Text or diagrams and their background must have a luminosity contrast ratio
		 * of at least 4.5:1 for level 2 conformance to guideline 1.4, and at least 7:1
		 * for level 3 conformance to guideline 1.4.
		 *
		 **/
		double luminosityContrastRatio = 0, luminosity1 = 0, luminosity2 = 0;

		luminosity1 = (rgb1[0] * 0.2126) + (rgb1[1] * 0.7152) + (rgb2[2] * 0.0722);
		luminosity2 = (rgb2[0] * 0.2126) + (rgb2[1] * 0.7152) + (rgb2[2] * 0.0722);

		luminosityContrastRatio = (luminosity1 + 0.5) / (luminosity2 + 0.5);

		return (int) luminosityContrastRatio;
	}

	public double luminosityContrastRatioW3C(int rgb1[], int rgb2[]) {
		/*
		 * (L1+.05) / (L2+.05) where L is luminosity and is defined as .2126*R + .7152*G
		 * + .0722B using linearised R, G, and B values. Linearised R (for example) =
		 * (R/FS)^2.2 where FS is full scale value (255 for 8 bit color channels). L1 is
		 * the higher value (of text or background) and L2 is the lower value.
		 *
		 * Text or diagrams and their background must have a luminosity contrast ratio
		 * of at least 4.5:1 for level 2 conformance to guideline 1.4, and at least 7:1
		 * for level 3 conformance to guideline 1.4.
		 *
		 **/
		double luminosityContrastRatio = 0f, luminosity1Temp = 0f, luminosity2Temp = 0f;
		double luminosity1 = 0f, luminosity2 = 0f;

		luminosity1Temp = getLuminosity(rgb1);
		luminosity2Temp = getLuminosity(rgb2);

		if (luminosity1Temp > luminosity2Temp) {
			luminosity1 = luminosity1Temp;
			luminosity2 = luminosity2Temp;
		} else {
			luminosity1 = luminosity2Temp;
			luminosity2 = luminosity1Temp;
		}

		luminosityContrastRatio = (luminosity1 + 0.05) / (luminosity2 + 0.05);
		luminosityContrastRatio = (double) Math.round(luminosityContrastRatio * 100) / (double) 100;

		return luminosityContrastRatio;
	}

	public double getLuminosity(int rgb[]) {
		double red = 0, green = 0, blue = 0, luminosityRed = 0, luminosityGreen = 0, luminosityBlue = 0, luminosity = 0;
		int iRed = 0, iGreen = 0, iBlue = 0;

		String hex = String.format("%02x%02x%02x", rgb[0], rgb[1], rgb[2]);
		iRed = Integer.parseInt(hex.substring(0, 2), 16);
		iGreen = Integer.parseInt(hex.substring(2, 4), 16);
		iBlue = Integer.parseInt(hex.substring(4, 6), 16);

		red = iRed / (double) 255;
		green = iGreen / (double) 255;
		blue = iBlue / (double) 255;

		luminosityRed = getColorLuminosity(red);
		luminosityGreen = getColorLuminosity(green);
		luminosityBlue = getColorLuminosity(blue);

		luminosity = (0.2126 * luminosityRed) + (0.7152 * luminosityGreen) + (0.0722 * luminosityBlue);

		return luminosity;
	}

	public double getColorLuminosity(Double colorValue) {
		double colorLuminosity = 0;

		if (colorValue <= 0.03928) {
			colorLuminosity = colorValue / 12.92;
		} else {
			colorLuminosity = Math.pow((colorValue + 0.055) / 1.055, 2.4);
		}

		return colorLuminosity;
	}
}
