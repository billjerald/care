package com.care.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.care.components.coloranalysis.Compute;
import com.care.components.imageproc.CreateImage;

public class ImageSurveyGenerator extends JFrame {
	private JLabel lblImageButton;
	private JLabel lblForegroundColorName;
	private JLabel lblForegroundRGB;
	private JButton btnGenerate;
	private JButton btnExtract;

	private Random rand = new Random();
	private ArrayList<Integer> fcolor = new ArrayList<>();
	private ArrayList<Integer> bcolor = new ArrayList<>();

	private int n, r, g, b, t;
	private double cbd = 0f, cd = 0f, lcr = 0f;
	private JLabel lblResultForegroundRGB;
	private JLabel lblResultForegroundColorName;

	private final String[] colorFileNames = { "color_names.csv", "colorhexa.csv", "x11_colors.csv" };
	private final String[] buttonText = { "APPLY", "DELETE", "ACCEPT", "CANCEL", "NEXT", "PROCEED", "DONE", "SIGN IN",
			"SIGN UP", "LOGIN", "LOG OUT", "ADD TO CART" };

	private JLabel lblForeground;
	private JLabel lblBackgroundColorName;
	private JLabel lblBackgroundRGB;
	private JLabel lblBackground;
	private JLabel lblResultBackgroundColorName;
	private JLabel lblResultBackgroundRGB;
	private JLabel lblFont;
	private JLabel lblStyle;
	private JLabel lblSize;
	private JComboBox comboBoxFont;
	private JComboBox comboBoxStyle;
	private JSpinner spinnerSize;

	private String fontChoice = "Open Sans";
	private int styleChoice = 1;
	private int sizeChoice = 35;
	private Font font = new Font(fontChoice, styleChoice, sizeChoice);
	private JLabel lblText;
	private JTextField textFieldText;
	private JLabel lblleaveItBlank;

	public Task task = new Task();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/Open_Sans/OpenSans-Regular.ttf")));
		} catch (IOException | FontFormatException e) {
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					ImageSurveyGenerator frame = new ImageSurveyGenerator();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// call Compute.java in coloranalysis
	Compute compute = new Compute();
	private JProgressBar progressBar;

	/**
	 * Create the frame.
	 */
	public ImageSurveyGenerator() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(650, 500);
		setLocationRelativeTo(null);
		setResizable(false);
		getContentPane().setLayout(null);

		lblImageButton = new JLabel("");
		// imageButton.setIcon(new
		// ImageIcon(ImageSurveyGenerator.class.getResource("/img/survey.png")));
		lblImageButton.setBounds(153, 34, 350, 100);
		getContentPane().add(lblImageButton);

		lblForegroundColorName = new JLabel("Color Name:");
		lblForegroundColorName.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblForegroundColorName.setBounds(123, 189, 125, 29);
		getContentPane().add(lblForegroundColorName);

		lblForegroundRGB = new JLabel("RGB:");
		lblForegroundRGB.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblForegroundRGB.setBounds(188, 217, 60, 29);
		getContentPane().add(lblForegroundRGB);

		btnGenerate = new JButton("Generate");
		btnGenerate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				task = new Task();
				task.addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evt) {
						if ("progress" == evt.getPropertyName()) {
							int progress = (Integer) evt.getNewValue();
							progressBar.setValue(progress);
						}
					}
				});
				task.execute();
			}
		});
		btnGenerate.setBounds(446, 437, 89, 23);
		getContentPane().add(btnGenerate);

		btnExtract = new JButton("Save");
		btnExtract.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setSelectedFile(new File("survey.png"));
				FileNameExtensionFilter filter = new FileNameExtensionFilter("Portable Network Graphics (.png)", "png");
				chooser.setFileFilter(filter);

				chooser.setDialogType(JFileChooser.SAVE_DIALOG);
				chooser.setDialogTitle("Save");

				int res = chooser.showSaveDialog(null);
				if (res == JFileChooser.APPROVE_OPTION) {
					final File selectedFile = chooser.getSelectedFile();
					String newFile = chooser.getSelectedFile().toString();
					if (!newFile.contains(".png")) {
						newFile = newFile + ".png";
					}
					if (new File(newFile).exists()) {
						new File(newFile).delete();
					}
					try {
						Files.copy(new File("res/img/survey.png").toPath(), new File(newFile).toPath());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		btnExtract.setBounds(545, 437, 89, 23);
		getContentPane().add(btnExtract);

		lblResultForegroundRGB = new JLabel("Unknown");
		lblResultForegroundRGB.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblResultForegroundRGB.setBounds(258, 217, 278, 29);
		getContentPane().add(lblResultForegroundRGB);

		lblResultForegroundColorName = new JLabel("Unknown");
		lblResultForegroundColorName.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblResultForegroundColorName.setBounds(258, 189, 278, 29);
		getContentPane().add(lblResultForegroundColorName);

		lblForeground = new JLabel("Foreground");
		lblForeground.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblForeground.setBounds(123, 161, 125, 29);
		getContentPane().add(lblForeground);

		lblBackgroundColorName = new JLabel("Color Name:");
		lblBackgroundColorName.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblBackgroundColorName.setBounds(123, 301, 125, 29);
		getContentPane().add(lblBackgroundColorName);

		lblBackgroundRGB = new JLabel("RGB:");
		lblBackgroundRGB.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblBackgroundRGB.setBounds(188, 329, 60, 29);
		getContentPane().add(lblBackgroundRGB);

		lblBackground = new JLabel("Background");
		lblBackground.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblBackground.setBounds(123, 273, 125, 29);
		getContentPane().add(lblBackground);

		lblResultBackgroundColorName = new JLabel("Unknown");
		lblResultBackgroundColorName.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblResultBackgroundColorName.setBounds(258, 301, 278, 29);
		getContentPane().add(lblResultBackgroundColorName);

		lblResultBackgroundRGB = new JLabel("Unknown");
		lblResultBackgroundRGB.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblResultBackgroundRGB.setBounds(258, 329, 278, 29);
		getContentPane().add(lblResultBackgroundRGB);

		lblFont = new JLabel("Font:");
		lblFont.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblFont.setBounds(10, 377, 125, 29);
		getContentPane().add(lblFont);

		lblStyle = new JLabel("Style:");
		lblStyle.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblStyle.setBounds(258, 377, 125, 29);
		getContentPane().add(lblStyle);

		lblSize = new JLabel("Size:");
		lblSize.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblSize.setBounds(466, 377, 60, 29);
		getContentPane().add(lblSize);

		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		comboBoxFont = new JComboBox(gEnv.getAvailableFontFamilyNames());
		comboBoxFont.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				fontChoice = comboBoxFont.getSelectedItem().toString();
			}
		});
		comboBoxFont.setBounds(60, 384, 175, 20);
		comboBoxFont.setSelectedItem(fontChoice);
		comboBoxFont.setMaximumRowCount(5);
		getContentPane().add(comboBoxFont);

		String[] styleNames = { "Plain", "Bold", "Italic", "Bold Italic" };
		comboBoxStyle = new JComboBox(styleNames);
		comboBoxStyle.setSelectedIndex(1);
		comboBoxStyle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				styleChoice = comboBoxStyle.getSelectedIndex();
			}
		});
		comboBoxStyle.setBounds(311, 384, 113, 20);
		getContentPane().add(comboBoxStyle);

		spinnerSize = new JSpinner();
		spinnerSize.setValue(35);
		spinnerSize.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				try {
					String size = spinnerSize.getModel().getValue().toString();
					sizeChoice = Integer.parseInt(size);
				} catch (NumberFormatException nfe) {
				}
			}
		});
		spinnerSize.setBounds(515, 384, 47, 20);
		getContentPane().add(spinnerSize);

		lblText = new JLabel("Text:");
		lblText.setFont(new Font("Open Sans", Font.PLAIN, 18));
		lblText.setBounds(10, 417, 125, 29);
		getContentPane().add(lblText);

		textFieldText = new JTextField();
		textFieldText.setBounds(60, 424, 175, 20);
		getContentPane().add(textFieldText);
		textFieldText.setColumns(10);

		lblleaveItBlank = new JLabel("(Leave it blank to generate default text.)");
		lblleaveItBlank.setBounds(60, 446, 213, 14);
		getContentPane().add(lblleaveItBlank);

		progressBar = new JProgressBar();
		progressBar.setBounds(0, 0, 644, 14);
		getContentPane().add(progressBar);
	}

	public static int[] convertIntegers(List<Integer> integers) {
		int[] ret = new int[integers.size()];
		Iterator<Integer> iterator = integers.iterator();
		for (int i = 0; i < ret.length; i++) {
			ret[i] = iterator.next().intValue();
		}
		return ret;
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			try {
				new File("res/img/colordetails.txt").delete();
				setProgress(0);
				for (int genBtn = 0; genBtn < 100; genBtn++) {
					String colorName = "";
					String temp = "";
					int i = -1;
					t = rand.nextInt(buttonText.length - 1);
					n = rand.nextInt(2187);
					r = rand.nextInt(255);
					g = rand.nextInt(255);
					b = rand.nextInt(255);

					// while (fcolor.contains(n)) {
					// n = rand.nextInt(2188);
					// }

					// fcolor.add(n);

					String rf = null, gf = null, bf = null;
					String rb = null, gb = null, bb = null;

					for (String fileName : colorFileNames) {
						File file = new File("res/colors/" + fileName);

						Scanner s = null;
						try {
							s = new Scanner(new BufferedReader(new FileReader(file)));
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						s.useDelimiter(",|\r\n|\r|\n");

						for (; s.hasNext() && i != n; i++) {
							colorName = s.next().trim();
							temp = s.next().trim();
							rf = s.next().trim();
							gf = s.next().trim();
							bf = s.next().trim();
						}
						s.close();
					}
					lblResultForegroundColorName.setText(colorName);
					lblResultForegroundRGB.setText(rf + " " + gf + " " + bf);

					int x = n;
					i = -1;
					n = rand.nextInt(2187);

					// while (x == n || bcolor.contains(n)) {
					// n = rand.nextInt(2188);
					// }

					// bcolor.add(n);

					for (String fileName : colorFileNames) {
						File file = new File("res/colors/" + fileName);

						Scanner s = null;
						try {
							s = new Scanner(new BufferedReader(new FileReader(file)));
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						s.useDelimiter(",|\r\n|\r|\n");

						for (; s.hasNext() && i != n; i++) {
							colorName = s.next().trim();
							temp = s.next().trim();
							rb = s.next().trim();
							gb = s.next().trim();
							bb = s.next().trim();
						}
						s.close();
					}

					lblResultBackgroundColorName.setText(colorName);
					lblResultBackgroundRGB.setText(rb + " " + gb + " " + bb);

					int fcolorArray[] = { Integer.parseInt(rf), Integer.parseInt(gf), Integer.parseInt(bf) };
					int bcolorArray[] = { Integer.parseInt(rb), Integer.parseInt(gb), Integer.parseInt(bb) };

					cbd = compute.colorBrightnessDifferenceW3C(fcolorArray, bcolorArray);
					cd = compute.colorDifference(fcolorArray, bcolorArray);
					lcr = compute.luminosityContrastRatioW3C(fcolorArray, bcolorArray);

					new CreateImage(new File("res/img/survey.png"),
							new Color(Integer.parseInt(rf), Integer.parseInt(gf), Integer.parseInt(bf)),
							new Color(Integer.parseInt(rb), Integer.parseInt(gb), Integer.parseInt(bb)),
							(textFieldText.getText().equals("")) ? buttonText[t] : textFieldText.getText(),
							new Font(fontChoice, styleChoice, sizeChoice));

					if (cbd >= 125 && cd >= 500 && lcr >= 4.5) {
						try {
							File details = new File("res/img/colordetails.txt");
							boolean bool = details.createNewFile();
							FileWriter fw = new FileWriter(details, true);

							System.out.println(genBtn + " " + cbd + " " + cd + " " + lcr);

							// Edit this to specify your own custom format for every line
							fw.append("survey" + genBtn + ".png" + " - Foreground: (" + rf + " " + gf + " " + bf
									+ ") Background: (" + rb + " " + gb + " " + bb + ") Color Brightness Difference: ("
									+ cbd + ") Color Difference: (" + cd + ") Luminosity Contrast Ratio: (" + lcr
									+ ")\r\n");

							fw.flush();
							fw.close();

							Files.copy(new File("res/img/survey.png").toPath(),
									new File("res/img/survey" + genBtn + ".png").toPath());

						} catch (IOException ex) {

						}
					} else {
						if (genBtn != 0) {
							genBtn--;
						}
					}

					BufferedImage img = null;
					try {
						img = ImageIO.read(new File("res/img/survey.png"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					lblImageButton.setIcon(null);
					lblImageButton.revalidate();
					lblImageButton.repaint();
					lblImageButton.update(lblImageButton.getGraphics());

					lblImageButton.setIcon(new ImageIcon(img));
					lblImageButton.revalidate();
					lblImageButton.repaint();
					lblImageButton.update(lblImageButton.getGraphics());

					setProgress(genBtn);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {

			setProgress(100);
		}
	}

	public void generateButtonColorAnalysis() {
		String colorName = "";
		String temp = "";
		int i = 1;
		t = rand.nextInt(buttonText.length - 1);
		n = rand.nextInt(2188);
		r = rand.nextInt(255);
		g = rand.nextInt(255);
		b = rand.nextInt(255);

		while (fcolor.contains(n)) {
			n = rand.nextInt(2188);
		}

		fcolor.add(n);

		String rf = null, gf = null, bf = null;
		String rb = null, gb = null, bb = null;

		for (String fileName : colorFileNames) {
			File file = new File("res/colors/" + fileName);

			Scanner s = null;
			try {
				s = new Scanner(new BufferedReader(new FileReader(file)));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			s.useDelimiter(",|\r\n|\r|\n");

			for (; s.hasNext() && i != n; i++) {
				colorName = s.next().trim();
				temp = s.next().trim();
				rf = s.next().trim();
				gf = s.next().trim();
				bf = s.next().trim();
			}
			s.close();
		}
		lblResultForegroundColorName.setText(colorName);
		lblResultForegroundRGB.setText(rf + " " + gf + " " + bf);

		int x = n;
		i = 1;
		n = rand.nextInt(2188);

		while (x == n || bcolor.contains(n)) {
			n = rand.nextInt(2188);
		}

		bcolor.add(n);

		// convert to int array to be able to use by coloranalysis
		int fcolorArray[] = convertIntegers(fcolor);
		int bcolorArray[] = convertIntegers(bcolor);

		cbd = compute.colorBrightnessDifferenceW3C(fcolorArray, bcolorArray);
		cd = compute.colorDifference(fcolorArray, bcolorArray);
		lcr = compute.luminosityContrastRatio(fcolorArray, bcolorArray);

		for (String fileName : colorFileNames) {
			File file = new File("res/colors/" + fileName);

			Scanner s = null;
			try {
				s = new Scanner(new BufferedReader(new FileReader(file)));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			s.useDelimiter(",|\r\n|\r|\n");

			for (; s.hasNext() && i != n; i++) {
				colorName = s.next().trim();
				temp = s.next().trim();
				rb = s.next().trim();
				gb = s.next().trim();
				bb = s.next().trim();
			}
			s.close();
		}
		lblResultBackgroundColorName.setText(colorName);
		lblResultBackgroundRGB.setText(rb + " " + gb + " " + bb);
		new CreateImage(new File("res/img/survey.png"),
				new Color(Integer.parseInt(rf), Integer.parseInt(gf), Integer.parseInt(bf)),
				new Color(Integer.parseInt(rb), Integer.parseInt(gb), Integer.parseInt(bb)),
				(textFieldText.getText().equals("")) ? buttonText[t] : textFieldText.getText(),
				new Font(fontChoice, styleChoice, sizeChoice));
		try {
			BufferedImage img = ImageIO.read(new File("res/img/survey.png"));
			lblImageButton.setIcon(new ImageIcon(img));
			lblImageButton.revalidate();
			lblImageButton.repaint();
			lblImageButton.update(lblImageButton.getGraphics());
		} catch (IOException ex) {

		}
	}
}
