package com.care.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import org.opencv.core.Core;

import com.care.components.imageproc.ImageProc;

public class Main extends JFrame {
	private static final long serialVersionUID = 1L;
	private JLabel lblPathFolder;
	private JButton btnEdit;
	private JButton btnApply;
	private JScrollPane scrollPane;
	private JLabel lblImages;
	private JFileChooser fileChooser;
	private JTextField textField;
	private File folder;
	private JTable table;
	private JProgressBar progressBar;
	private Task task;
	private JTabbedPane tabbedPane;
	private PrintStream standardOut;
	private JScrollPane scrollPane_1;
	private JTextArea textArea;

	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	private String[] column = { "Filename", "Result" };

	private Object[][] data = {};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// String opencvpath = System.getProperty("user.dir") + "\\lib\\";
		// String libPath = System.getProperty("java.library.path");
		// System.load(opencvpath + Core.NATIVE_LIBRARY_NAME + ".dll");
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setSize(1000, 720);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblPathFolder = new JLabel("Path Folder:");
		lblPathFolder.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblPathFolder.setBounds(35, 31, 72, 20);
		getContentPane().add(lblPathFolder);

		btnEdit = new JButton("Edit");
		btnEdit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fileChooser.showDialog(getContentPane(), "Open");
				textField.setText(fileChooser.getSelectedFile().toString());
			}
		});
		btnEdit.setBounds(582, 30, 89, 23);
		getContentPane().add(btnEdit);

		btnApply = new JButton("Apply");
		btnApply.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnApply.setEnabled(false);
				// Instances of javax.swing.SwingWorker are not reusuable, so
				// we create new instances as needed.

				task = new Task();
				task.addPropertyChangeListener(new PropertyChangeListener() {
					@Override
					public void propertyChange(PropertyChangeEvent evt) {
						if ("progress" == evt.getPropertyName()) {
							int progress = (Integer) evt.getNewValue();
							progressBar.setValue(progress);
						}
					}
				});
				task.execute();

			}
		});
		btnApply.setBounds(681, 30, 89, 23);
		getContentPane().add(btnApply);

		textField = new JTextField();
		textField.setBounds(109, 31, 461, 19);
		getContentPane().add(textField);
		textField.setColumns(10);

		progressBar = new JProgressBar();
		progressBar.setBounds(35, 377, 732, 20);
		getContentPane().add(progressBar);
		progressBar.setStringPainted(true);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(35, 62, 735, 304);
		getContentPane().add(tabbedPane);

		scrollPane = new JScrollPane();
		tabbedPane.addTab("Result", null, scrollPane, null);

		lblImages = new JLabel("Images");
		lblImages.setHorizontalAlignment(SwingConstants.CENTER);
		scrollPane.setColumnHeaderView(lblImages);

		table = new JTable(data, column);
		table.getColumnModel().getColumn(0).setPreferredWidth(400);
		scrollPane.setViewportView(table);

		scrollPane_1 = new JScrollPane();
		tabbedPane.addTab("Console Log", null, scrollPane_1, null);

		textArea = new JTextArea();
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);
		textArea.setEditable(false);
		scrollPane_1.setViewportView(textArea);
		PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));

		// keeps reference of standard output stream
		setStandardOut(System.out);

		// re-assigns standard output strceam and error output stream
		System.setOut(printStream);
		System.setErr(printStream);

	}

	public void setData(String[] listOfImages, String[] listOfResults) {
		Object[][] data = new Object[listOfImages.length][2];
		int f = 0;

		for (String file : listOfImages) {
			data[f][0] = file;
			data[f][1] = listOfResults[f];
			f++;
		}
		table = new JTable(data, column);
		table.getColumnModel().getColumn(0).setPreferredWidth(400);
		table.setDefaultEditor(Object.class, null);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				// JTable table = (JTable) e.getSource();
				// Point point = e.getPoint();
				// int row = table.rowAtPoint(point);
				if (e.getClickCount() == 2 && table.getSelectedRow() != -1) {
					// your valueChanged overridden method
					setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
					System.out.println(table.getValueAt(table.getSelectedRow(), 0).toString());
					String image = table.getValueAt(table.getSelectedRow(), 0).toString().contains(".jpg") ? ".jpg"
							: ".png";
					new Details(table.getValueAt(table.getSelectedRow(), 0).toString(), image).setVisible(true);
					setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		scrollPane.setViewportView(table);
	}

	public String[] increaseArray(String[] theArray) {
		int x = theArray.length;
		int n = ++x;
		String[] newArray = new String[n];
		for (int i = 0; i < theArray.length; i++) {
			newArray[i] = theArray[i];
		}
		return newArray;
	}

	public PrintStream getStandardOut() {
		return standardOut;
	}

	public void setStandardOut(PrintStream standardOut) {
		this.standardOut = standardOut;
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			folder = new File(textField.getText());
			File[] listOfFiles = folder.listFiles();
			final String[][] listOfImages = { new String[0] };
			final String[][] listOfResults = { new String[0] };
			double count = 0;
			int i = 0;
			int progress = 0;
			setProgress(0);

			ImageProc newsp = new ImageProc();

			for (File file : listOfFiles) {
				if (file.isFile() && (file.getName().contains(".jpg") || file.getName().contains(".png"))) {
					System.out.println(file.getAbsolutePath());

					String image = file.getName().contains(".jpg") ? "jpg" : "png";

					newsp.processImage(file.getAbsolutePath(), image);
					listOfResults[0] = increaseArray(listOfResults[0]);
					listOfImages[0] = increaseArray(listOfImages[0]);
					listOfImages[0][i] = file.getAbsolutePath();
					listOfResults[0][i] = Double.toString(newsp.getAverage());
					i++;
				}
				count++;
				progress = (int) ((count / listOfFiles.length) * 100f);
				setProgress(progress);
			}
			setData(listOfImages[0], listOfResults[0]);

			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			btnApply.setEnabled(true);
			setProgress(100);
		}
	}
}
