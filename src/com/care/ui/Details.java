package com.care.ui;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import com.care.components.imageproc.ImageProc;

public class Details extends JFrame {

	private static final long serialVersionUID = 1L;

	private String[] column = { "Elements No.", "1st Color", "2nd Color", "Color Brightness Difference",
			"Color Difference", "Luminosity Contrast Ratio", "Result" };

	private Object[][] data = {};
	private JLabel lblNumberOfElements;
	private JLabel lblLuminosityContrast;
	private JLabel lblColorDifference;
	private JLabel lblColorBrightnessDiff;
	private JScrollPane scrollPane;
	private JTable table;
	private JLabel lblElements;
	private JLabel lblLuminosity;
	private JLabel lblColorDiff;
	private JLabel lblColorBrightnessDifference;

	/**
	 * Create the frame.
	 */
	public Details(String path, String image) {
		ImageProc newsp = new ImageProc();
		newsp.processImage(path, image);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setResizable(false);
		setSize(1000, 600);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		lblNumberOfElements = new JLabel("Number of Elements:");
		lblNumberOfElements.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNumberOfElements.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNumberOfElements.setBounds(295, 27, 139, 19);
		getContentPane().add(lblNumberOfElements);

		lblLuminosityContrast = new JLabel("Luminosity Contrast:");
		lblLuminosityContrast.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLuminosityContrast.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLuminosityContrast.setBounds(295, 57, 139, 19);
		getContentPane().add(lblLuminosityContrast);

		lblColorDifference = new JLabel("Color Difference:");
		lblColorDifference.setHorizontalAlignment(SwingConstants.RIGHT);
		lblColorDifference.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblColorDifference.setBounds(295, 87, 139, 19);
		getContentPane().add(lblColorDifference);

		lblColorBrightnessDifference = new JLabel("Color Brightness Difference:");
		lblColorBrightnessDifference.setHorizontalAlignment(SwingConstants.RIGHT);
		lblColorBrightnessDifference.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblColorBrightnessDifference.setBounds(295, 87, 139, 19);
		getContentPane().add(lblColorDifference);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(36, 116, 920, 411);
		getContentPane().add(scrollPane);

		table = new JTable(data, column);
		scrollPane.setViewportView(table);

		lblElements = new JLabel(Integer.toString(newsp.getElements()));
		lblElements.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblElements.setBounds(444, 29, 512, 14);
		getContentPane().add(lblElements);

		lblLuminosity = new JLabel();
		lblLuminosity.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLuminosity.setBounds(444, 59, 512, 14);
		getContentPane().add(lblLuminosity);

		lblColorDiff = new JLabel();
		lblColorDiff.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblColorDiff.setBounds(444, 91, 512, 14);
		getContentPane().add(lblColorDiff);
		table.getColumnModel().getColumn(0).setPreferredWidth(30);

		lblColorBrightnessDiff = new JLabel();
		lblColorBrightnessDiff.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblColorBrightnessDiff.setBounds(444, 91, 512, 14);
		getContentPane().add(lblColorBrightnessDiff);
		table.getColumnModel().getColumn(0).setPreferredWidth(30);

		setData(newsp.getColor1(), newsp.getColor2(), newsp.getElements(), newsp.getColorBDList(),
				newsp.getColorDList(), newsp.getColorLCRList(), newsp.getResultListDeuteranomaly());
	}

	public void setData(ArrayList<int[]> color1, ArrayList<int[]> color2, int size, ArrayList<Integer> colorBDList,
			ArrayList<Integer> colorDList, ArrayList<Double> colorLCRList, ArrayList<String> resultList) {

		Object[][] data = new Object[size][7];

		for (int i = 0; i < size; i++) {
			data[i][0] = i + 1;
			data[i][1] = color1.get(i)[0] + ", " + color1.get(i)[1] + ", " + color1.get(i)[2];
			data[i][2] = color2.get(i)[0] + ", " + color2.get(i)[1] + ", " + color2.get(i)[2];
			data[i][3] = colorBDList.get(i);
			data[i][4] = colorDList.get(i);
			data[i][5] = colorLCRList.get(i);
			data[i][6] = resultList.get(i);
		}

		table = new JTable(data, column);
		table.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table);
	}
}
