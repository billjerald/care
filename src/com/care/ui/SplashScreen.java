package com.care.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import com.care.components.imageproc.ImageProc;

public class SplashScreen extends JFrame {

	private JPanel contentPane;
	private JLabel lblEyeIcon;
	private JLabel lblCare;
	private JLabel lblColorblind;
	private JLabel lblFolderIcon;
	private JLabel lblOpenFolder;
	private JLabel lblClickToChoose;
	private JLabel lblPlaceholder;
	private JFileChooser fileChooser;
	private Task task;
	private Timer timer;

	private String directory = null;
	private JProgressBar progressBar;
	private JLabel lblStatus;

	private String[][] images = null;
	private ArrayList<Object[][]> submatData = new ArrayList<>();
	private ArrayList<Mat> imgWithRects = new ArrayList<>();
	private ArrayList<String> imgWithRectsFiles = new ArrayList<>();;

	private boolean isDirectorySelected = false;

	static {
		try {
			System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		} catch (UnsatisfiedLinkError e) {
			try {
				NativeUtils.loadLibraryFromJar("/" + Core.NATIVE_LIBRARY_NAME + ".dll"); // during runtime. .DLL within
																							// .JAR
			} catch (IOException e1) {
				throw new RuntimeException(e1);
			}
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					new File("res/fonts/NokiaHeadline/NokiaPureHeadline-Bold.ttf")));
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					new File("res/fonts/NokiaHeadline/NokiaPureHeadline-ExtraBold.ttf")));
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					new File("res/fonts/NokiaHeadline/NokiaPureHeadline-Light.ttf")));
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					new File("res/fonts/NokiaHeadline/NokiaPureHeadline-Regular.ttf")));
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,
					new File("res/fonts/NokiaHeadline/NokiaPureHeadline-UltraLight.ttf")));
			ge.registerFont(
					Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/NokiaPure/NokiaPureText-Bold.ttf")));
			ge.registerFont(
					Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/NokiaPure/NokiaPureText-Light.ttf")));
			ge.registerFont(
					Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/NokiaPure/NokiaPureText-Medium.ttf")));
			ge.registerFont(
					Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/NokiaPure/NokiaPureText-Regular.ttf")));
		} catch (IOException | FontFormatException e) {
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					SplashScreen frame = new SplashScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SplashScreen() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(SplashScreen.class.getResource("/img/icon.png")));
		setTitle("Colorblind Accessibility Review and Evaluation Tool");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(100, 100, 670, 450);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(72, 168, 76));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblCare = new JLabel("CARE");
		lblCare.setForeground(Color.WHITE);
		lblCare.setFont(new Font("Nokia Pure Headline Light", Font.PLAIN, 120));
		lblCare.setBounds(215, 25, 382, 139);
		contentPane.add(lblCare);

		lblColorblind = new JLabel("<html>Colorblind Accessibility Review and Evaluation Tool</html>");
		lblColorblind.setForeground(Color.WHITE);
		lblColorblind.setFont(new Font("Nokia Pure Headline Light", Font.PLAIN, 20));
		lblColorblind.setBounds(215, 155, 429, 34);
		contentPane.add(lblColorblind);

		lblEyeIcon = new JLabel("");
		lblEyeIcon.setHorizontalTextPosition(SwingConstants.CENTER);
		lblEyeIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblEyeIcon.setIcon(new ImageIcon(SplashScreen.class.getResource("/img/icon.png")));
		lblEyeIcon.setBounds(10, 0, 199, 211);
		contentPane.add(lblEyeIcon);

		lblPlaceholder = new JLabel("");
		lblPlaceholder.setForeground(Color.WHITE);
		lblPlaceholder.setFont(new Font("Nokia Pure Headline Light", Font.PLAIN, 18));
		lblPlaceholder.setHorizontalTextPosition(SwingConstants.CENTER);
		lblPlaceholder.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlaceholder.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				if (isDirectorySelected == false) {
					fileChooser = new JFileChooser();
					fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
					fileChooser.showDialog(getContentPane(), "Open");

					try {
						directory = fileChooser.getSelectedFile().toString();
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					if (!directory.equals(null)) {
						task = new Task();
						task.addPropertyChangeListener(new PropertyChangeListener() {
							@Override
							public void propertyChange(PropertyChangeEvent evt) {
								if ("progress" == evt.getPropertyName()) {
									int progress = (Integer) evt.getNewValue();
									progressBar.setValue(progress);
								}
							}
						});
						task.execute();
					}
				}
			}
		});
		lblPlaceholder.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblPlaceholder.setBounds(0, 188, 664, 175);
		contentPane.add(lblPlaceholder);

		lblFolderIcon = new JLabel("");
		lblFolderIcon.setHorizontalTextPosition(SwingConstants.CENTER);
		lblFolderIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblFolderIcon.setIcon(new ImageIcon(new ImageIcon(SplashScreen.class.getResource("/img/opened-folder.png"))
				.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT)));
		lblFolderIcon.setBounds(117, 245, 92, 70);
		contentPane.add(lblFolderIcon);

		lblOpenFolder = new JLabel("Open Folder");
		lblOpenFolder.setForeground(Color.WHITE);
		lblOpenFolder.setFont(new Font("Nokia Pure Headline Light", Font.PLAIN, 20));
		lblOpenFolder.setBounds(215, 251, 429, 34);
		contentPane.add(lblOpenFolder);

		lblClickToChoose = new JLabel("Click to choose a working directory");
		lblClickToChoose.setForeground(Color.WHITE);
		lblClickToChoose.setFont(new Font("Nokia Pure Headline Ultra Light", Font.PLAIN, 16));
		lblClickToChoose.setBounds(215, 281, 429, 34);
		contentPane.add(lblClickToChoose);

		progressBar = new JProgressBar();
		progressBar.setBorderPainted(false);
		progressBar.setBorder(null);
		progressBar.setVisible(false);

		lblStatus = new JLabel();
		lblStatus.setForeground(Color.WHITE);
		lblStatus.setFont(new Font("Nokia Pure Headline Ultra Light", Font.PLAIN, 16));
		lblStatus.setBounds(10, 369, 654, 27);
		contentPane.add(lblStatus);
		progressBar.setForeground(Color.GREEN);
		progressBar.setBounds(-1, 407, 665, 14);
		contentPane.add(progressBar);
	}

	public String[] increaseArray(String[] theArray) {
		int x = theArray.length;
		int n = ++x;
		String[] newArray = new String[n];
		for (int i = 0; i < theArray.length; i++) {
			newArray[i] = theArray[i];
		}
		return newArray;
	}

	class Task extends SwingWorker<Void, Void> {
		/*
		 * Main task. Executed in background thread.
		 */
		@Override
		public Void doInBackground() {
			isDirectorySelected = true;
			lblFolderIcon.setVisible(false);
			lblOpenFolder.setVisible(false);
			lblClickToChoose.setVisible(false);
			lblPlaceholder.setText("<html><center>Loading. Please wait.</center></html>");
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					lblPlaceholder.setText("<html><center>Did you know: " + setTrivia() + "</center></html>");
				}
			}, 1000, 10000);
			lblPlaceholder.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			progressBar.setVisible(true);
			lblStatus.setText("Checking directory");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}

			File folder = new File(directory);
			File[] listOfFiles = folder.listFiles();
			final String[][] listOfImages = { new String[0] };
			final String[][] listOfResults = { new String[0] };
			double count = 0;
			final int[] i = { 0 };
			int progress = 0;
			setProgress(0);

			ImageProc imgProc = null;

			lblStatus.setText("Looking for image files");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}

			for (File file : listOfFiles) {
				if (file.isFile() && (file.getName().contains(".jpg") || file.getName().contains(".png"))) {
					lblStatus.setText("Processing Image: " + file.getName());
					String image = file.getName().contains(".jpg") ? "jpg" : "png";
					imgProc = new ImageProc();
					try {
						imgProc.processImage(file.getAbsolutePath(), image);
						imgWithRects.add(imgProc.getImageRect());
						imgWithRectsFiles.add(file.getName());
						submatData.add(setData(imgProc.getColor1(), imgProc.getColor2(), imgProc.getElements(),
								imgProc.getColorBDList(), imgProc.getColorDList(), imgProc.getColorLCRList(),
								imgProc.getResultListDeuteranomaly(), imgProc.getResultListProtanomaly()));
					} catch (Exception ex) {
						ex.printStackTrace();
					}

					listOfResults[0] = increaseArray(listOfResults[0]);
					listOfImages[0] = increaseArray(listOfImages[0]);
					listOfImages[0][i[0]] = file.getName();
					listOfResults[0][i[0]] = Double.toString(imgProc.getAverage());
					i[0]++;
				}
				count++;
				progress = (int) ((count / listOfFiles.length) * 100f);
				setProgress(progress);
			}

			images = listOfImages;

			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			setProgress(100);
			lblStatus.setText("Done");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
			}
			dispose();
			new MainFrame(images, imgWithRects, imgWithRectsFiles, submatData, directory).setVisible(true);
		}
	}

	public Object[][] setData(ArrayList<int[]> color1, ArrayList<int[]> color2, int size,
			ArrayList<Integer> colorBDList, ArrayList<Integer> colorDList, ArrayList<Double> colorLCRList,
			ArrayList<String> resultListDeuteranomaly, ArrayList<String> resultListProtanomaly) {

		Object[][] data = new Object[size][8];

		for (int i = 0; i < size; i++) {
			data[i][0] = i + "";
			data[i][1] = color1.get(i)[0] + ", " + color1.get(i)[1] + ", " + color1.get(i)[2];
			data[i][2] = color2.get(i)[0] + ", " + color2.get(i)[1] + ", " + color2.get(i)[2];
			data[i][3] = colorBDList.get(i);
			data[i][4] = colorDList.get(i);
			data[i][5] = colorLCRList.get(i);
			data[i][6] = resultListDeuteranomaly.get(i);
			data[i][7] = resultListProtanomaly.get(i);
		}

		return data;
	}

	private String setTrivia() {
		InputStream dataset = getClass().getResourceAsStream("/trivia.txt");
		// new File("/trivia.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(dataset));
		Random rand = new Random();
		Scanner s = null;
		s = new Scanner(reader);

		s.useDelimiter("\r\n|\r|\n");

		int i = 0, num = rand.nextInt(49);
		String temp = null;

		while (s.hasNext()) {
			temp = s.next();
			if (i == num) {
				break;
			}
			i++;
		}
		s.close();

		return temp;
	}

	void deleteDir(File file) {
		File[] contents = file.listFiles();
		if (contents != null) {
			for (File f : contents) {
				if (!Files.isSymbolicLink(f.toPath())) {
					deleteDir(f);
				}
			}
		}
		file.delete();
	}
}
