package com.care.ui.TreeTable;

import java.util.ArrayList;
import java.util.Arrays;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;

public class TreeTable {

	private String[] headings = { "Image File", "Elements" };
	private Node root;
	private DefaultTreeTableModel model;
	private JXTreeTable table;
	private ArrayList<String[]> content;

	public TreeTable(ArrayList<String[]> content) {
		this.content = content;
	}

	public JXTreeTable getTreeTable() {
		root = new RootNode("Root");

		ChildNode myChild = null;
		for (String[] data : this.content) {
			ChildNode child = new ChildNode(data);
			if (data.length <= 1) {
				root.add(child);
				myChild = child;
			} else {
				myChild.add(child);
			}
		}

		model = new DefaultTreeTableModel(root, Arrays.asList(headings));
		table = new JXTreeTable(model);
		table.setShowGrid(true, true);
		// table.setColumnControlVisible(true);

		table.packAll();

		return table;
	}

}