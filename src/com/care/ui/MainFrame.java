package com.care.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.AbstractMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.opencv.core.Mat;

import com.care.components.imageproc.ImageProc;
import com.care.ui.TreeTable.ChildNode;
import com.care.ui.TreeTable.Node;
import com.care.ui.TreeTable.RootNode;

public class MainFrame extends JFrame {

	private JPanel contentPane;
	private JXTreeTable treeTable;

	private String[] headings = { "Image File" };
	private Node root;
	private DefaultTreeTableModel model;
	private JXTreeTable table;
	private String[][] images;
	private String[][] submat;
	private ArrayList<Object[][]> submatData;
	private JScrollPane scrollPane;
	private JLabel lblWorkingDirectory;
	private JLabel lblImage;
	private String directory;
	private JPanel panel;
	private JLabel lblReady;
	private JLabel lblImageRects;
	private List<Mat> imgRects;
	private ArrayList<String> imgRectsFiles;
	private JPanel panel_1;
	private JLabel lbl1stColor;
	private JLabel lbl2ndColor;
	private JLabel lblColorBrightnessDifference;
	private JLabel lblColorDifference;
	private JLabel lblLuminosityConstrastRatio;
	private JLabel lblResultDeuteranomaly;
	private JPanel panel_2;
	private JPanel panel_3;
	private JLabel lblResultProtanomaly;

	/*
	 * static { try { System.loadLibrary(Core.NATIVE_LIBRARY_NAME); } catch
	 * (UnsatisfiedLinkError e) { try { NativeUtils.loadLibraryFromJar("/" +
	 * Core.NATIVE_LIBRARY_NAME + ".dll"); // during runtime. .DLL within // .JAR }
	 * catch (IOException e1) { throw new RuntimeException(e1); } } }
	 */

	/**
	 * Create the frame.
	 */
	public MainFrame(String[][] listofImages, List<Mat> imgWithRects, ArrayList<String> imgWithRectsFiles,
			ArrayList<Object[][]> listSubmatData, String dir) {
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/img/icon.png")));
		setTitle("Colorblind Accessibility Review and Evaluation Tool");

		images = getImages(listofImages);
		submatData = listSubmatData;
		directory = dir;
		imgRects = imgWithRects;
		imgRectsFiles = imgWithRectsFiles;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 620);
		setResizable(false);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblWorkingDirectory = new JLabel(
				"<html>Working Directory: " + directory + "	<u>Click here to change</u></html>");
		lblWorkingDirectory.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				dispose();
				new SplashScreen().setVisible(true);
			}
		});
		lblWorkingDirectory.setBounds(20, 11, 918, 48);
		contentPane.add(lblWorkingDirectory);
		lblWorkingDirectory.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblWorkingDirectory.setHorizontalAlignment(SwingConstants.LEFT);
		lblWorkingDirectory.setForeground(Color.BLACK);
		lblWorkingDirectory.setFont(new Font("Nokia Pure Headline Light", Font.PLAIN, 16));
		try {
			lblWorkingDirectory.setIcon(new ImageIcon(new ImageIcon(new File("res/img/pc_icon.png").toURI().toURL())
					.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT)));
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 70, 300, 480);
		contentPane.add(scrollPane);

		treeTable = getTreeTable();
		treeTable.setBounds(21, 70, 300, 600);
		scrollPane.setViewportView(treeTable);

		lblImage = new JLabel("");
		lblImage.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblImage.setBounds(350, 70, 280, 480);
		contentPane.add(lblImage);
		lblImage.setHorizontalTextPosition(SwingConstants.CENTER);
		lblImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblImage.setPreferredSize(new Dimension(280, 480));

		lblImageRects = new JLabel("");
		lblImageRects.setVisible(false);
		lblImageRects.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblImageRects.setPreferredSize(new Dimension(280, 480));
		lblImageRects.setHorizontalTextPosition(SwingConstants.CENTER);
		lblImageRects.setHorizontalAlignment(SwingConstants.CENTER);
		lblImageRects.setBounds(680, 70, 280, 480);
		contentPane.add(lblImageRects);

		panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setVgap(1);
		flowLayout.setAlignment(FlowLayout.LEFT);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel.setBounds(0, 571, 994, 19);
		contentPane.add(panel);

		lblReady = new JLabel("Ready");
		lblReady.setHorizontalTextPosition(SwingConstants.LEFT);
		lblReady.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(lblReady);

		panel_1 = new JPanel();
		panel_1.setVisible(false);
		panel_1.setBounds(660, 70, 300, 480);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		lbl1stColor = new JLabel("1st Color:");
		lbl1stColor.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lbl1stColor.setBounds(10, 25, 260, 22);
		panel_1.add(lbl1stColor);

		lbl2ndColor = new JLabel("2nd Color:");
		lbl2ndColor.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lbl2ndColor.setBounds(10, 85, 260, 22);
		panel_1.add(lbl2ndColor);

		lblColorBrightnessDifference = new JLabel("Color Brightness Difference:");
		lblColorBrightnessDifference.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lblColorBrightnessDifference.setBounds(10, 150, 260, 22);
		panel_1.add(lblColorBrightnessDifference);

		lblColorDifference = new JLabel("Color Difference:");
		lblColorDifference.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lblColorDifference.setBounds(10, 215, 260, 22);
		panel_1.add(lblColorDifference);

		lblLuminosityConstrastRatio = new JLabel("Luminosity Constrast Ratio:");
		lblLuminosityConstrastRatio.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lblLuminosityConstrastRatio.setBounds(10, 280, 260, 22);
		panel_1.add(lblLuminosityConstrastRatio);

		lblResultDeuteranomaly = new JLabel("Result (Deuteranomaly):");
		lblResultDeuteranomaly.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lblResultDeuteranomaly.setBounds(10, 345, 260, 22);
		panel_1.add(lblResultDeuteranomaly);

		panel_2 = new JPanel();
		panel_2.setBounds(75, 52, 195, 22);
		panel_1.add(panel_2);

		panel_3 = new JPanel();
		panel_3.setBounds(75, 110, 195, 22);
		panel_1.add(panel_3);

		lblResultProtanomaly = new JLabel("Result (Protanomaly):");
		lblResultProtanomaly.setFont(new Font("Nokia Pure Headline", Font.PLAIN, 16));
		lblResultProtanomaly.setBounds(10, 408, 260, 22);
		panel_1.add(lblResultProtanomaly);
		BufferedImage bimg = null;
		try {
			bimg = ImageIO.read(getClass().getResourceAsStream("/img/icon.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// panel_1.add(new ScalablePane(bimg), BorderLayout.CENTER);

		if (bimg.getHeight() > 480 || bimg.getWidth() > 280) {
			lblImage.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/img/icon.png")).getImage()
					.getScaledInstance(280, 480, Image.SCALE_DEFAULT)));
		} else {
			lblImage.setIcon(new ImageIcon(new ImageIcon(getClass().getResource("/img/icon.png")).getImage()
					.getScaledInstance(bimg.getWidth(), bimg.getHeight(), Image.SCALE_DEFAULT)));
		}
	}

	public JXTreeTable getTreeTable() {
		root = new RootNode("Root");
		ChildNode myChild = null;
		int i = 0;
		for (String[] data : images) {
			ChildNode child = new ChildNode(data);
			root.add(child);
			myChild = child;
			submat = getSubmat(submatData, i, data[0].contains(".jpg") ? ".jpg" : ".png");
			for (String[] dataSubmat : submat) {
				child = new ChildNode(dataSubmat);
				myChild.add(child);
			}
			i++;
		}

		model = new DefaultTreeTableModel(root, Arrays.asList(headings));
		table = new JXTreeTable(model);
		table.setOpenIcon(new ImageIcon(new ImageIcon(MainFrame.class.getResource("/img/imageIcon.png")).getImage()
				.getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
		table.setClosedIcon(new ImageIcon(new ImageIcon(MainFrame.class.getResource("/img/imageIcon.png")).getImage()
				.getScaledInstance(16, 16, Image.SCALE_SMOOTH)));
		table.setOverwriteRendererIcons(true);
		table.setEditable(false);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSelectionAllowed(false);
		table.setHorizontalScrollEnabled(true);
		table.setLargeModel(true);
		table.setShowGrid(true, true);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (table.getSelectedRow() != -1) {
					AbstractMutableTreeTableNode node = (AbstractMutableTreeTableNode) table
							.getPathForRow(table.getSelectedRow()).getLastPathComponent();

					if (node.isLeaf()) {
						String[] parent = (String[]) node.getParent().getUserObject();
						BufferedImage bimg = null;
						try {
							bimg = ImageIO.read(new File(
									directory + "\\submatFiles\\" + parent[0].replace(".jpg", "").replace(".png", "")
											+ "\\" + table.getValueAt(table.getSelectedRow(), 0).toString()));
						} catch (IOException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
						try {
							if (bimg.getHeight() > 480 && bimg.getWidth() > 280) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(directory + "\\submatFiles\\"
										+ parent[0].replace(".jpg", "").replace(".png", "") + "\\"
										+ table.getValueAt(table.getSelectedRow(), 0).toString()).toURI().toURL())
												.getImage().getScaledInstance(280, 480, Image.SCALE_SMOOTH)));
							} else if (bimg.getHeight() > 480) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(directory + "\\submatFiles\\"
										+ parent[0].replace(".jpg", "").replace(".png", "") + "\\"
										+ table.getValueAt(table.getSelectedRow(), 0).toString()).toURI().toURL())
												.getImage()
												.getScaledInstance(bimg.getWidth(), 480, Image.SCALE_SMOOTH)));
							} else if (bimg.getWidth() > 280) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(directory + "\\submatFiles\\"
										+ parent[0].replace(".jpg", "").replace(".png", "") + "\\"
										+ table.getValueAt(table.getSelectedRow(), 0).toString()).toURI().toURL())
												.getImage()
												.getScaledInstance(280, bimg.getHeight(), Image.SCALE_SMOOTH)));
							} else {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(directory + "\\submatFiles\\"
										+ parent[0].replace(".jpg", "").replace(".png", "") + "\\"
										+ table.getValueAt(table.getSelectedRow(), 0).toString()).toURI().toURL())
												.getImage().getScaledInstance(bimg.getWidth(), bimg.getHeight(),
														Image.SCALE_SMOOTH)));
							}

							lbl1stColor.setText("1st Color: "
									+ (String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node
											.getParent().getIndex(node)][1]);

							panel_2.setBackground(getColorFromString(
									(String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node.getParent()
											.getIndex(node)][1]));

							lbl2ndColor.setText("2nd Color: "
									+ (String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node
											.getParent().getIndex(node)][2]);

							panel_3.setBackground(getColorFromString(
									(String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node.getParent()
											.getIndex(node)][2]));

							lblColorBrightnessDifference.setText("Color Brightness Difference: "
									+ (int) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node.getParent()
											.getIndex(node)][3]);

							lblColorDifference.setText("Color Difference: "
									+ (int) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node.getParent()
											.getIndex(node)][4]);

							lblLuminosityConstrastRatio.setText("Luminosity Contrast Ratio: "
									+ (double) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node
											.getParent().getIndex(node)][5]);

							lblResultDeuteranomaly.setText("Result (Deuteranomaly): "
									+ (String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node
											.getParent().getIndex(node)][6]);

							lblResultProtanomaly.setText("Result (Protanomaly): "
									+ (String) submatData.get(getImgRectIndex(imgRectsFiles, parent[0]))[node
											.getParent().getIndex(node)][7]);

							panel_1.setVisible(true);
							lblImageRects.setVisible(false);

						} catch (MalformedURLException ex) {
							ex.printStackTrace();
						}
					} else {
						BufferedImage bimg = null;
						try {
							bimg = ImageIO.read(
									new File(directory + "/" + table.getValueAt(table.getSelectedRow(), 0).toString()));
						} catch (IOException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
						try {
							if (bimg.getHeight() > 480 && bimg.getWidth() > 280) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(
										directory + "/" + table.getValueAt(table.getSelectedRow(), 0).toString())
												.toURI().toURL()).getImage().getScaledInstance(280, 480,
														Image.SCALE_SMOOTH)));
								lblImageRects.setIcon(new ImageIcon(new ImageIcon(
										new ImageProc().matToBufferedImage2(imgRects.get(getImgRectIndex(imgRectsFiles,
												table.getValueAt(table.getSelectedRow(), 0).toString())))).getImage()
														.getScaledInstance(280, 480, Image.SCALE_SMOOTH)));
							} else if (bimg.getHeight() > 480) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(
										directory + "/" + table.getValueAt(table.getSelectedRow(), 0).toString())
												.toURI().toURL()).getImage().getScaledInstance(bimg.getWidth(), 480,
														Image.SCALE_SMOOTH)));
								lblImageRects.setIcon(new ImageIcon(new ImageIcon(
										new ImageProc().matToBufferedImage2(imgRects.get(getImgRectIndex(imgRectsFiles,
												table.getValueAt(table.getSelectedRow(), 0).toString())))).getImage()
														.getScaledInstance(bimg.getWidth(), 480, Image.SCALE_SMOOTH)));
							} else if (bimg.getWidth() > 280) {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(
										directory + "/" + table.getValueAt(table.getSelectedRow(), 0).toString())
												.toURI().toURL()).getImage().getScaledInstance(280, bimg.getHeight(),
														Image.SCALE_SMOOTH)));
								lblImageRects.setIcon(new ImageIcon(new ImageIcon(
										new ImageProc().matToBufferedImage2(imgRects.get(getImgRectIndex(imgRectsFiles,
												table.getValueAt(table.getSelectedRow(), 0).toString())))).getImage()
														.getScaledInstance(280, bimg.getHeight(), Image.SCALE_SMOOTH)));
							} else {
								lblImage.setIcon(new ImageIcon(new ImageIcon(new File(
										directory + "/" + table.getValueAt(table.getSelectedRow(), 0).toString())
												.toURI().toURL()).getImage().getScaledInstance(bimg.getWidth(),
														bimg.getHeight(), Image.SCALE_SMOOTH)));
								lblImageRects.setIcon(new ImageIcon(new ImageIcon(
										new ImageProc().matToBufferedImage2(imgRects.get(getImgRectIndex(imgRectsFiles,
												table.getValueAt(table.getSelectedRow(), 0).toString())))).getImage()
														.getScaledInstance(bimg.getWidth(), bimg.getHeight(),
																Image.SCALE_SMOOTH)));
							}

							panel_1.setVisible(false);
							lblImageRects.setVisible(true);

						} catch (MalformedURLException ex) {
							ex.printStackTrace();
						}
					}
				}
			}
		});
		table.packAll();

		return table;
	}

	public String[][] getImages(String[][] listofImages) {
		String[][] data = new String[listofImages[0].length][1];

		for (int i = 0; i < listofImages[0].length; i++) {
			data[i][0] = listofImages[0][i];
		}

		return data;
	}

	public String[][] getSubmat(ArrayList<Object[][]> listSubmatData, int index, String imageType) {
		String[][] data = new String[listSubmatData.get(index).length][1];

		for (int i = 0; i < listSubmatData.get(index).length; i++) {
			data[i][0] = "submat" + ((String) listSubmatData.get(index)[i][0]) + imageType;
		}

		return data;
	}

	private int getImgRectIndex(ArrayList<String> imgWithRectsFiles, String filename) {
		int i = 0;

		for (String s : imgWithRectsFiles) {
			if (s.equals(filename)) {
				break;
			}
			i++;
		}

		return i;
	}

	private Color getColorFromString(String rgb) {
		String[] color = rgb.split(",");

		int r = Integer.parseInt(color[0].trim());
		int g = Integer.parseInt(color[1].trim());
		int b = Integer.parseInt(color[2].trim());

		return new Color(r, g, b);
	}
}
